# Argument Explorer Gephi
This repository contains a set of Gephi Plugins designed to aid the visualisation of Abstract Argumentation Frameworks.

It consists of four plugins:

*  **Argumentation Solver Plugin:** Allows integration with external Argumentation Solvers
*  **Graph Condensation Plugin:** Creates condensations of graphs based on arbitrary attributes
*  **Graph Travsersal Plugin:** Expand and Contract Node neighbours to traverse a graph
*  **Framework Importer Plugin:** Allows the Import of TGF and APX files


## Building
Each plugin needs to be built individually, and the artifacts will be placed in a target folder for each module. For convenience, I have set up a Maven profile for each plugin

The commands required to build are:

```
mvn package -P argumentationSolver
mvn package -P graphCondensation
mvn package -P graphTraversal
mvn package -P frameworkImporter
```

Building requires the use of Maven 3.6 and JDK 9+