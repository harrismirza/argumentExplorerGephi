package org.harrismirza.argumentExplorer.argumentationsolver;

import java.util.Objects;

/**
 * Class that stores data about a Docker Image
 */
public class DockerImageMetadata {
    public String repository;
    public String tag;
    public String imageId;
    public String created;
    public String size;

    public DockerImageMetadata(String repository, String tag, String imageId, String created, String size) {
        this.repository = repository;
        this.tag = tag;
        this.imageId = imageId;
        this.created = created;
        this.size = size;
    }

    @Override
    public String toString() {
        return String.format("%s:%s %s, created %s, %s", repository, tag, imageId, created, size);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DockerImageMetadata that = (DockerImageMetadata) o;
        return Objects.equals(repository, that.repository) &&
                Objects.equals(tag, that.tag) &&
                Objects.equals(imageId, that.imageId) &&
                Objects.equals(created, that.created) &&
                Objects.equals(size, that.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(repository, tag, imageId, created, size);
    }
}
