package org.harrismirza.argumentExplorer.argumentationsolver;

import org.gephi.statistics.spi.Statistics;
import org.gephi.statistics.spi.StatisticsBuilder;
import org.openide.util.lookup.ServiceProvider;


/**
 * Builder class that creates instances of the {@link org.harrismirza.argumentExplorer.argumentationsolver.ArgumentationSolverStatistic} class
 */
@ServiceProvider(service = StatisticsBuilder.class)
public class ArgumentationSolverStatisticBuilder implements StatisticsBuilder {
    @Override
    public String getName() {
        return "Argumentation Solver";
    }

    @Override
    public Statistics getStatistics() {
        return new ArgumentationSolverStatistic();
    }

    @Override
    public Class<? extends Statistics> getStatisticsClass() {
        return ArgumentationSolverStatistic.class;
    }
}
