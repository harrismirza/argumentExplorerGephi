package org.harrismirza.argumentExplorer.argumentationsolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Argumentation Solver interface for Docker-based solvers
 */
public class DockerArgumentationSolverInterface extends ArgumentationSolverInterface {

    public DockerArgumentationSolverInterface(String solverPath) {
        super(solverPath);

    }

    @Override
    public String prepareFilePath(String filePath) {
        // Add folder prefix to file path, as docker only allows mounting folders rather than copying single files
        return "/graph_files/" + filePath;
    }

    @Override
    public String callSolver(boolean verifyOutput, String... arguments) throws IOException {
        System.out.println("**************");
        System.out.println("Args: " + String.join(",", arguments));
        // Construct args for docker (using the docker run command)
        List<String> dockerRunArgs = Arrays.asList("docker", "run", "-v", String.format("%s:/graph_files",
                tempDirectory), "--rm", solverPath, ".", String.join(" ", arguments));
        System.out.println("Full Docker Command : " + String.join(",", dockerRunArgs));
        Process dockerCommand = new ProcessBuilder(dockerRunArgs).start();
        InputStream dockerOutput = dockerCommand.getInputStream();
        String dockerOutputString =
                (new BufferedReader(new InputStreamReader(dockerOutput)).lines().collect(Collectors.joining("\n")));

        InputStream dockerError = dockerCommand.getErrorStream();
        String dockerErrorString =
                (new BufferedReader(new InputStreamReader(dockerError)).lines().collect(Collectors.joining("\n")));

        System.out.println(dockerOutputString);
        System.out.println(dockerErrorString);
        System.out.println(dockerCommand.isAlive());
        System.out.println("**************");

        // Validate the solver output, if the output is not "YES" or "NO", then it must contain a square bracket
        if (verifyOutput && (dockerOutputString.isEmpty() || (!(dockerOutputString.equals("YES") || dockerOutputString.equals("NO")) && !dockerOutputString.contains("[")))) {
            throw new IOException("Container returned invalid output: \nSTDOUT:\n" + dockerOutputString + "\nSTDERR" +
                    ":\n" + dockerErrorString);
        }

        return dockerOutputString;
    }
}
