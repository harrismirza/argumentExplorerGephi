package org.harrismirza.argumentExplorer.argumentationsolver;

import org.gephi.statistics.spi.Statistics;
import org.gephi.statistics.spi.StatisticsUI;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


/**
 * The options dialog displayed when loading the Argumentation Solver Statistic
 */
@ServiceProvider(service = StatisticsUI.class)
public class ArgumentationSolverStatisticUI implements StatisticsUI {

    private ArgumentationSolverStatistic statistic;

    private JComboBox<String> problemsComboBox;

    private Throwable threadError;

    @Override
    public JPanel getSettingsPanel() {
        JPanel settingsPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        //Row 1 - Description Label
        JLabel descriptionLabel = new JLabel("Please specify whether your solver is an executable or a docker image " +
                "and then enter the path/docker image name.");
        descriptionLabel.setHorizontalAlignment(JLabel.CENTER);
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 3;
        gbc.weightx = 3;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.PAGE_START;
        settingsPanel.add(descriptionLabel, gbc);

        //Row 2 - Solver Type Radio Buttons
        JLabel solverTypeLabel = new JLabel("Solver Type:");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        settingsPanel.add(solverTypeLabel, gbc);

        JRadioButton executableButton = new JRadioButton("Executable");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.FIRST_LINE_END;
        settingsPanel.add(executableButton, gbc);

        JRadioButton dockerButton = new JRadioButton("Docker");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.FIRST_LINE_END;
        settingsPanel.add(dockerButton, gbc);

        ButtonGroup solverTypeButtons = new ButtonGroup();
        solverTypeButtons.add(executableButton);
        solverTypeButtons.add(dockerButton);
        solverTypeButtons.setSelected(executableButton.getModel(), true);

        //Row 3 - Solver Text Entry/Combo Box
        JTextField solverPathTextField = new JTextField();
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 2;
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        settingsPanel.add(solverPathTextField, gbc);
        JComboBox<DockerImageMetadata> dockerImageComboBox = new JComboBox<>();
        dockerImageComboBox.setVisible(false);
        settingsPanel.add(dockerImageComboBox, gbc);

        JButton browseButton = new JButton("Browse");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.FIRST_LINE_END;
        settingsPanel.add(browseButton, gbc);

        //Row 4 - Query Button
        JButton queryButton = new JButton("Query Solver");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.PAGE_START;
        settingsPanel.add(queryButton, gbc);

        //Row 5 - Problem Description Label
        JLabel problemDescriptionLabel = new JLabel("Please query the solver to get the list of problems it can solve" +
                ".");
        problemDescriptionLabel.setHorizontalAlignment(JLabel.CENTER);
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 3;
        gbc.weightx = 3;
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.PAGE_END;
        settingsPanel.add(problemDescriptionLabel, gbc);

        //Row 6 - Problem Selection
        JLabel problemSelectionLabel = new JLabel("Problem:");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.LAST_LINE_START;
        settingsPanel.add(problemSelectionLabel, gbc);

        problemsComboBox = new JComboBox<>();
        problemsComboBox.setEnabled(false);
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 2;
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.LAST_LINE_END;
        settingsPanel.add(problemsComboBox, gbc);

        // When radio buttons are selected, store the state and enable/disable the browse button.
        ActionListener solverButtonsActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (solverTypeButtons.isSelected(executableButton.getModel())) {
                    browseButton.setEnabled(true);
                    solverPathTextField.setVisible(true);
                    dockerImageComboBox.setVisible(false);
                } else {
                    browseButton.setEnabled(false);
                    solverPathTextField.setVisible(false);
                    dockerImageComboBox.setVisible(true);

                    // Get docker images
                    final JDialog waitingDialog = new JDialog();
                    JPanel waitingDialogPanel = new JPanel();
                    waitingDialogPanel.add(new JLabel("Getting Available Docker Images..."));
                    waitingDialog.getContentPane().add(waitingDialogPanel);

                    waitingDialog.pack();
                    waitingDialog.setLocationRelativeTo(settingsPanel);
                    waitingDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                    waitingDialog.setModal(true);

                    Thread t = new Thread(() -> {
                        List<DockerImageMetadata> dockerImages = null;
                        try {
                            dockerImages = (DockerImagesInterface.getDockerImageMetaData());
                        } catch (IOException | InterruptedException ex) {
                            threadError = (ex);
                            if(threadError != null){
                                waitingDialog.dispose();
                                JOptionPane.showMessageDialog(settingsPanel, threadError.getMessage(), "Docker Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        if(dockerImages != null) {
                            final List<DockerImageMetadata> finalDockerImages = dockerImages;
                            SwingUtilities.invokeLater(() -> {
                                waitingDialog.dispose();
                                dockerImageComboBox.setModel(new DefaultComboBoxModel<>(finalDockerImages.toArray(new DockerImageMetadata[0])));

                            });
                        }
                    });

                    t.start();

                    waitingDialog.setVisible(true);
                }
            }
        };

        dockerImageComboBox.addActionListener(e -> {
            problemsComboBox.setModel(new DefaultComboBoxModel<>());
            problemsComboBox.setEnabled(false);
        });

        executableButton.addActionListener(solverButtonsActionListener);

        dockerButton.addActionListener(solverButtonsActionListener);

        JFileChooser fileChooser = new JFileChooser();
        // Setup the browse button to use the file chooser

        browseButton.addActionListener(e -> {
            fileChooser.showOpenDialog(settingsPanel);
            File chosenSolver = fileChooser.getSelectedFile();
            if (chosenSolver != null) {
                solverPathTextField.setText(chosenSolver.getAbsolutePath());
            }
            problemsComboBox.setModel(new DefaultComboBoxModel<>());
            problemsComboBox.setEnabled(false);
        });

        // When the query button is pressed, query the solver and then populate the list of problems
        queryButton.addActionListener((e) -> {
            if (dockerButton.isSelected()) {
                DockerImageMetadata selectedImage = ((DockerImageMetadata)dockerImageComboBox.getSelectedItem());
                statistic.setSolver(new DockerArgumentationSolverInterface(selectedImage.imageId));
            } else {
                statistic.setSolver(new ExecutableArgumentationSolverInterface(solverPathTextField.getText()));
            }

            try {
                statistic.getSolver().querySolverInfo();
                Set<String> problemSet = statistic.getSolver().getSupportedProblems();
                String[] problems = problemSet.toArray(new String[0]);
                Arrays.sort(problems);
                problemsComboBox.setModel(new DefaultComboBoxModel<>(problems));
                statistic.setProblem(problems[0]);
                problemsComboBox.setEnabled(true);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(settingsPanel, "Error querying solver: " + ex.getMessage(), "Solver " +
                        "error", JOptionPane.ERROR_MESSAGE);
            }
        });

        problemsComboBox.addActionListener((e) -> statistic.setProblem(problemsComboBox.getItemAt(problemsComboBox.getSelectedIndex())));

        return settingsPanel;
    }

    @Override
    public void setup(Statistics statistics) {
        this.statistic = (ArgumentationSolverStatistic) statistics;
    }

    @Override
    public void unsetup() {
        this.statistic = null;
    }

    @Override
    public Class<? extends Statistics> getStatisticsClass() {
        return ArgumentationSolverStatistic.class;
    }

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public String getDisplayName() {
        return "Argumentation Solver";
    }

    @Override
    public String getShortDescription() {
        return "Short Description";
    }

    @Override
    public String getCategory() {
        return StatisticsUI.CATEGORY_NODE_OVERVIEW;
    }

    @Override
    public int getPosition() {
        return 10000;
    }
}
