package org.harrismirza.argumentExplorer.argumentationsolver;

public enum ArgumentationSemantics {
    Complete("CO"),
    Preferred("PR"),
    Stable("ST"),
    SemiStable("SST"),
    Stage("STG"),
    Grounded("GR"),
    Ideal("ID"),
    ;

    private String abbreviation;

    ArgumentationSemantics(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public static ArgumentationSemantics getSemanticsFromAbbreviation(String abbreviation) {
        for (ArgumentationSemantics value : values()) {
            if (value.abbreviation.equalsIgnoreCase(abbreviation)) {
                return value;
            }
        }
        return null;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
