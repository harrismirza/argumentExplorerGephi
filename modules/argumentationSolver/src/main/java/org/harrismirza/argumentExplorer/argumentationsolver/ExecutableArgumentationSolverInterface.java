package org.harrismirza.argumentExplorer.argumentationsolver;

import java.io.*;
import java.util.stream.Collectors;

/**
 * The Argumentation Solver interface for executable-based solvers
 */
public class ExecutableArgumentationSolverInterface extends ArgumentationSolverInterface {
    public ExecutableArgumentationSolverInterface(String solverPath) {
        super(solverPath);
    }

    @Override
    public String prepareFilePath(String filePath) {
        return filePath;
    }

    @Override
    public String callSolver(boolean verifyOutput, String... arguments) throws IOException {
        // Construct arguments to run the solver
        String[] fullArguments = new String[arguments.length + 1];
        System.arraycopy(arguments, 0, fullArguments, 1, arguments.length);
        fullArguments[0] = super.solverPath;
        Process solverProcess = new ProcessBuilder(fullArguments).directory(new File(tempDirectory)).start();

        InputStream solverOutput = solverProcess.getInputStream();
        String solverOutputString =
                (new BufferedReader(new InputStreamReader(solverOutput)).lines().collect(Collectors.joining("")));

        InputStream solverError = solverProcess.getErrorStream();
        String solverErrorString =
                (new BufferedReader(new InputStreamReader(solverError)).lines().collect(Collectors.joining("")));

        // Validate the solver output, if the output is not "YES" or "NO", then it must contain a square bracket
        if (verifyOutput && (solverOutputString.isEmpty() || (!(solverOutputString.equals("YES") || solverOutputString.equals("NO")) && !solverOutputString.contains("[")))) {
            throw new IOException("Solver returned invalid output: \nSTDOUT:\n" + solverOutputString + "\nSTDERR:\n" + solverErrorString);
        }

        return solverOutputString;
    }
}
