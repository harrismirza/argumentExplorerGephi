package org.harrismirza.argumentExplorer.argumentationsolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Class that can query a local Docker configuration for available images
 */
class DockerImagesInterface {
     static List<DockerImageMetadata> getDockerImageMetaData() throws IOException, InterruptedException {
         Process dockerImagesCommand = new ProcessBuilder("docker", "images").start();
         boolean exitedNormally = dockerImagesCommand.waitFor(20, TimeUnit.SECONDS);
         if(!exitedNormally){
             throw new IOException("Docker timed out.");
         }
         InputStream dockerImagesOutput = dockerImagesCommand.getInputStream();
         List<String> dockerImagesOutputLines= (new BufferedReader(new InputStreamReader(dockerImagesOutput))
                 .lines().collect(Collectors.toList()));

         if(dockerImagesCommand.isAlive()){
             dockerImagesCommand.destroyForcibly();
         }

         // Parse output
         if(dockerImagesOutputLines.isEmpty()){
             throw new IOException("Docker returned no output");
         }

         return parseDockerImagesOutput(dockerImagesOutputLines);

     }

     static List<DockerImageMetadata> parseDockerImagesOutput(List<String> output) throws IOException {
         // Find lengths of each segment
         Pattern firstLinePattern = Pattern.compile("(REPOSITORY\\s*)(TAG\\s*)(IMAGE ID\\s*)(CREATED\\s*)(SIZE\\s*)");
         Matcher m = firstLinePattern.matcher(output.get(0));
         if(!m.matches()){
             throw new IOException(String.format("Docker returned invalid output: %s", String.join("\n", output)));
         }
         int repoLength = m.group(1).length();
         int tagLength = m.group(2).length();
         int idLength = m.group(3).length();
         int createdLength = m.group(4).length();

         ArrayList<DockerImageMetadata> dockerImages = new ArrayList<>();

         for (int i = 1; i < output.size(); i++) {
             String line = output.get(i);
             line = line.trim();
             if(!line.isEmpty()){
                 // Parse the line
                 String repo = line.substring(0, repoLength).trim();
                 String tag = line.substring(repoLength, repoLength + tagLength).trim();
                 String id = line.substring(repoLength + tagLength, repoLength + tagLength + idLength).trim();
                 String created = line.substring(repoLength + tagLength + idLength, repoLength + tagLength + idLength + createdLength).trim();
                 String size = line.substring(repoLength + tagLength + idLength + createdLength).trim();

                 DockerImageMetadata imageMetadata = new DockerImageMetadata(repo, tag, id, created, size);
                 dockerImages.add(imageMetadata);
             }
         }

         return dockerImages;
     }
}
