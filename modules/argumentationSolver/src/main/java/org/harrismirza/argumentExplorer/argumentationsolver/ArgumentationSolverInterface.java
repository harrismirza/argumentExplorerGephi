package org.harrismirza.argumentExplorer.argumentationsolver;

import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Element;
import org.gephi.graph.api.Node;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class interfaces with the solver programs. It controls the configuration of the solver, supplying the graph as input, and getting the correct output depending on the selected problem type.
 */
public abstract class ArgumentationSolverInterface {
    // This directory is where temporary Graph Files will be stored as they are being processed by the solver
    final static String tempDirectory = System.getProperty("java.io.tmpdir") + "tempframeworkfiles/";

    //These are lists of solver problems that the program can parse
    private static Set<String> supportedSolverProblemsSE = new HashSet<>(Arrays.asList(
            "SE-CO", "SE-PR", "SE-ST", "SE-SST", "SE-STG", "SE-GR", "SE-ID"));
    private static Set<String> supportedSolverProblemsEE = new HashSet<>(Arrays.asList(
            "EE-CO", "EE-PR", "EE-ST", "EE-SST", "EE-STG", "EE-GR", "EE-ID"));
    private static Set<String> supportedSolverProblemsDC = new HashSet<>(Arrays.asList(
            "DC-CO", "DC-PR", "DC-ST", "DC-SST", "DC-STG", "DC-GR", "DC-ID"));
    private static Set<String> supportedSolverProblemsDS = new HashSet<>(Arrays.asList(
            "DS-CO", "DS-PR", "DS-ST", "DS-SST", "DS-STG", "DS-GR", "DS-ID"));

    String solverPath;
    private String solverInfo;
    private Set<String> supportedFormats;
    private Set<String> supportedProblems;
    private boolean useTGF = true;

    ArgumentationSolverInterface(String solverPath) {
        this.solverPath = solverPath;
        this.supportedFormats = new HashSet<>();
        this.supportedProblems = new HashSet<>();
        File directory = new File(tempDirectory);
        directory.mkdir();
    }

    /**
     * This method should call the solver executable with the supplied arguments and return the output as a string. If verifyOutput is selected, it should ensure that the output is not empty and contains a list of arguments, a YES or a NO string.
     * @param verifyOutput Whether the output should be verified
     * @param arguments The arguments to provide to the solver program
     * @return The solver output
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    abstract String callSolver(boolean verifyOutput, String... arguments) throws IOException;

    /**
     * Modifies the file path of the graph file given to the solver
     * @param filePath The original file path
     * @return The updated file path
     */
    abstract String prepareFilePath(String filePath);

    /**
     * Queries the solver to get supported problems and formats. Selects whether to use APX or TGF (prefer TGF)
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    void querySolverInfo() throws IOException {
        // Get the solver info (name and creator) as in ICCMA Spec
        String solverInfo = callSolver(false);
        this.solverInfo = solverInfo;

        // Parse the supported formats and set which to use (prioritise TGF as it has a smaller file size)
        String formatString = callSolver(true,"--formats");
        System.out.println(formatString);
        supportedFormats.addAll(Arrays.asList(formatString.substring(1, formatString.length() - 1).replaceAll(" ",
                "").split(",")));
        if (!supportedFormats.contains("tgf")) {
            useTGF = false;
        }

        // Get supported problems
        String problemString = callSolver(true,"--problems");
        System.out.println(problemString);
        supportedProblems.addAll(Arrays.asList(problemString.substring(1, problemString.length() - 1).replaceAll(" ",
                "").split(",")));

        // Use set operations to find intersection of the problems the solver and this program supports
        Set<String> supportedSolverProblems = new HashSet<>(supportedSolverProblemsSE);
        supportedSolverProblems.addAll(supportedSolverProblemsEE);
        supportedSolverProblems.addAll(supportedSolverProblemsDC);
        supportedSolverProblems.addAll(supportedSolverProblemsDS);
        supportedProblems.retainAll(supportedSolverProblems);

        // Work out which DS and DC via EE proxy problems are supported
        Set<String> supportedEEProblems = new HashSet<>(supportedProblems);
        supportedEEProblems.retainAll(supportedSolverProblemsEE);

        for (String supportedEEProblem : supportedEEProblems) {
            String semanticSuffix = supportedEEProblem.substring(2);
            supportedProblems.add("DC(EE)" + semanticSuffix);
            supportedProblems.add("DS(EE)" + semanticSuffix);
        }

        System.out.println(solverInfo);
        System.out.println(supportedFormats);
        System.out.println(supportedProblems);
    }

    /**
     * Calls the solver to solve a single extension
     * @param argumentationGraph The graph to solve
     * @param semantics The semantics to solve with
     * @return A mapping of argument ids to the generated labelling
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    Map<Object, SolverLabelling> solveSomeExtension(DirectedGraph argumentationGraph,
                                                    ArgumentationSemantics semantics) throws IOException {
        String filePath = generateTempFile(argumentationGraph);
        String extension = callSolver(true,"-p",
                ArgumentationProblems.SomeExtension.getAbbreviation() + "-" + semantics.getAbbreviation(), "-f",
                filePath, "-fo", useTGF ? "tgf" : "apx");

        // Handle case where no extension is found
        if (extension.equalsIgnoreCase("NO")) {
            return null;
        }
        return convertExtensionToLabelling(argumentationGraph, extension.substring(1, extension.length() - 1));
    }

    /**
     * Calls the solver to solve all extensions
     * @param argumentationGraph The graph to solve
     * @param semantics The semantics to solve with
     * @return A list of mappings of argument ids to the generated labellings, one for each extension
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    List<Map<Object, SolverLabelling>> solveEnumerateExtension(DirectedGraph argumentationGraph,
                                                               ArgumentationSemantics semantics) throws IOException {
        String filePath = generateTempFile(argumentationGraph);
        String solverString = callSolver(true,"-p",
                ArgumentationProblems.EnumerateExtension.getAbbreviation() + "-" + semantics.getAbbreviation(), "-f",
                filePath, "-fo", useTGF ? "tgf" : "apx");

        // Handle case where no extensions are found
        if (solverString.equalsIgnoreCase("[]")) {
            return null;
        }
        // Parse out the multiple extensions
        String[] extensions = solverString.substring(2, solverString.length() - 2).split("],?\n?\\[");

        List<Map<Object, SolverLabelling>> labellings = new ArrayList<>();
        // Store labellings in a list
        for (String extension : extensions) {
            System.out.println(extension);
            Map<Object, SolverLabelling> labelling = convertExtensionToLabelling(argumentationGraph, extension);
            labellings.add(labelling);
        }
        return labellings;
    }

    /**
     * Converts a string representation of an extension to a mapping from argument ids the corresponding labelling
     * @param argumentationGraph The graph the extension was generated from
     * @param extension The extension to convert
     * @return The generated labelling
     */
    private Map<Object, SolverLabelling> convertExtensionToLabelling(DirectedGraph argumentationGraph,
                                                                     String extension) {
        //Get extension arguments from extension string
        String[] acceptedArguments = extension.split(",");
        Map<Object, SolverLabelling> extensionLabelling = new HashMap<>();

        // Mark all arguments that are in the extension as 'In'
        for (String acceptedArgument : acceptedArguments) {
            extensionLabelling.put(acceptedArgument, SolverLabelling.IN);
        }

        // For each node, if any of it's predecessors are 'In', mark as 'Out', else mark as 'Undec'
        argumentationGraph.getNodes().forEach(node -> {
            if (!extensionLabelling.containsKey(node.getId().toString())) {
                boolean out = false;

                for (Node parentNode : argumentationGraph.getPredecessors(node)) {
                    if (extensionLabelling.getOrDefault(parentNode.getId().toString(), SolverLabelling.UNDEC) == SolverLabelling.IN) {
                        extensionLabelling.put(node.getId().toString(), SolverLabelling.OUT);
                        out = true;
                        break;
                    }
                }
                if (!out) {
                    extensionLabelling.put(node.getId().toString(), SolverLabelling.UNDEC);
                }
            }
        });

        return extensionLabelling;
    }

    /**
     * Solves the acceptance of each node in the graph, uses the dedicated decision option in the solvers
     * @param argumentationGraph The graph to solve
     * @param semantics The semantics to solve with
     * @param credulous True to solve for a credulous acceptance or false for a sceptical acceptance
     * @return A mapping of argument ids to the generated decision
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    Map<Object, AcceptanceLabelling> solveAcceptance(DirectedGraph argumentationGraph,
                                                     ArgumentationSemantics semantics, boolean credulous) throws IOException {
        String filePath = generateTempFile(argumentationGraph);
        Map<Object, AcceptanceLabelling> acceptance = new HashMap<>();
        for (Node node : argumentationGraph.getNodes().toArray()) {
            // Loop over all nodes and call the solver to find acceptance
            String solverString = callSolver(true,"-p", (credulous ? ArgumentationProblems.DecideCredulous :
                    ArgumentationProblems.DecideSceptical).getAbbreviation() + "-" + semantics.getAbbreviation(), "-f"
                    , filePath, "-fo", useTGF ? "tgf" : "apx", "-a", node.getId().toString());
            if (solverString.equalsIgnoreCase("YES")) {
                acceptance.put(node.getId(), AcceptanceLabelling.YES);
            } else if (solverString.equalsIgnoreCase("NO")) {
                acceptance.put(node.getId(), AcceptanceLabelling.NO);
            }
        }
        return acceptance;
    }

    /**
     * Solves the acceptance of each node in the graph, by first enumerating all extensions and then using set operations
     * @param argumentationGraph The graph to solve
     * @param semantics The semantics to solve with
     * @param credulous True to solve for a credulous acceptance or false for a sceptical acceptance
     * @return A mapping of argument ids to the generated decision
     * @throws IOException Thrown when there is an error communicating with the solver
     */
    Map<Object, AcceptanceLabelling> solveAcceptanceViaExtensions(DirectedGraph argumentationGraph,
                                                                  ArgumentationSemantics semantics,
                                                                  boolean credulous) throws IOException {
        String filePath = generateTempFile(argumentationGraph);
        String solverString = callSolver(true,"-p",
                ArgumentationProblems.EnumerateExtension.getAbbreviation() + "-" + semantics.getAbbreviation(), "-f",
                filePath, "-fo", useTGF ? "tgf" : "apx");
        Set<Object> acceptanceSet = new HashSet<>();
        List<Object> nodes =
                argumentationGraph.getNodes().toCollection().stream().map(Element::getId).collect(Collectors.toList());

        // Parse all extensions unless the solver has no extensions found
        if (!solverString.equals("[]")){
            String[] extensions = solverString.substring(2, solverString.length() - 2).split("],?\n?\\[");
            // If solving sceptical acceptance, initialise acceptance set with all arguments
            if (!credulous) {
                acceptanceSet.addAll(nodes);
            }
            // For each extension,
            // if credulous: add all the extension arguments in the extension to the acceptance set,
            // if sceptical: retain all of the extension arguments in the acceptance set
            for (String extension : extensions) {
                List<Object> extensionList = Arrays.asList(extension.split(","));
                if (credulous) {
                    acceptanceSet.addAll(extensionList);
                } else {
                    acceptanceSet.retainAll(extensionList);
                }
            }
        }
        // Convert the acceptance set to mapping
        Map<Object, AcceptanceLabelling> acceptance = new HashMap<>();
        for (Object node : nodes) {
            acceptance.put(node, acceptanceSet.contains(node) ? AcceptanceLabelling.YES : AcceptanceLabelling.NO);
        }
        return acceptance;
    }

    /**
     * Generates a temporary graph file to pass into the solver.
     * @param directedGraph The graph to create the graph file from
     * @return The path to the generated file
     * @throws IOException Thrown when the file cannot be created
     */
    private String generateTempFile(DirectedGraph directedGraph) throws IOException {
        // Generate a temporary file of the right format
        return prepareFilePath(useTGF ? generateTempTGFFile(directedGraph) : generateTempAPXFile(directedGraph));
    }

    private String generateTempAPXFile(DirectedGraph directedGraph) throws IOException {
        // Make APX String
        StringBuilder apxBuilder = new StringBuilder();
        // Add Nodes
        directedGraph.getNodes().forEach(node -> apxBuilder.append("arg(").append(node.getId().toString()).append(")" +
                ".\n"));
        // Add Edges
        directedGraph.getEdges().forEach(edge -> apxBuilder.append("att(").append(edge.getSource().getId().toString()).append(",").append(edge.getTarget().getId().toString()).append(").\n"));
        // Get file path
        String fileName = "/temp.apx";
        BufferedWriter tgfWriter = new BufferedWriter(new FileWriter(tempDirectory + fileName));
        tgfWriter.write(apxBuilder.toString());
        tgfWriter.close();
        return fileName;
    }

    private String generateTempTGFFile(DirectedGraph directedGraph) throws IOException {
        // Make TGF String
        StringBuilder tgfBuilder = new StringBuilder();
        // Add Nodes
        directedGraph.getNodes().forEach(node -> tgfBuilder.append(node.getId().toString()).append("\n"));
        // Add separator
        tgfBuilder.append("#\n");
        // Add Edges
        directedGraph.getEdges().forEach(edge -> tgfBuilder.append(edge.getSource().getId().toString()).append(" ").append(edge.getTarget().getId().toString()).append("\n"));
        // Get file path
        String fileName = "/temp.tgf";
        BufferedWriter tgfWriter = new BufferedWriter(new FileWriter(tempDirectory + fileName));
        tgfWriter.write(tgfBuilder.toString());
        tgfWriter.close();
        return fileName;
    }

    String getSolverInfo() {
        return solverInfo;
    }

    Set<String> getSupportedFormats() {
        return supportedFormats;
    }

    Set<String> getSupportedProblems() {
        return supportedProblems;
    }

    enum SolverLabelling {
        IN,
        OUT,
        UNDEC
    }

    enum AcceptanceLabelling {
        YES,
        NO
    }
}