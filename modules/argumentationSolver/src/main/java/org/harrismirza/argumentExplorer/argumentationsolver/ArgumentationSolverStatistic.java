package org.harrismirza.argumentExplorer.argumentationsolver;

import org.gephi.graph.api.*;
import org.gephi.statistics.spi.Statistics;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


/**
 * This class implements the statistic for the Argumentation Solver integration, it manages the options for the solver interface, and writes the results to the graph
 */
public class ArgumentationSolverStatistic implements Statistics {

    public boolean computed = false;
    // Keep a reference to the exception thrown within the thread so it can be displayed
    private AtomicReference<Throwable> executionException = new AtomicReference<>();
    private ArgumentationSolverInterface solver;
    private String error = "";
    private String problem;

    /**
     * Sets the selected problem string for this instance of the statistic
     *
     * @param problem The ICCMA problem string that the solver should use
     */
    public void setProblem(String problem) {
        this.problem = problem;
    }

    /**
     * This executes the solver and writes the results to the graph
     *
     * @param graphModel The graph model containing the graph
     */
    @Override
    public void execute(GraphModel graphModel) {
        System.out.println("Executing");
        if(problem == null || problem.isEmpty()){
            error = "No problem selected";
            return;
        }
        Thread t = new Thread(() -> {
            DirectedGraph graph = graphModel.getDirectedGraphVisible();

            //Get Problem and Semantics from String
            String[] splitProblem = problem.split("-");
            String problemString = splitProblem[0];
            String semanticsString = splitProblem[1];


            // Convert the problem strings to enum values
            ArgumentationProblems problem = ArgumentationProblems.getProblemFromAbbreviation(problemString);
            ArgumentationSemantics semantics = ArgumentationSemantics.getSemanticsFromAbbreviation(semanticsString);


            try {
                switch (problem) {
                    // If the problem to be solved is the some extension problem, call the solver and write the
                    // labelling to a column in the node table
                    case SomeExtension:
                        Map<Object, ArgumentationSolverInterface.SolverLabelling> labellingMap =
                                solver.solveSomeExtension(graph, semantics);
                        if (labellingMap == null) {
                            error = "No extension exists for these semantics.";
                        } else {
                            System.out.println(labellingMap);
                            writeLabellingToColumn(semantics.name(), graph, labellingMap);
                        }
                        break;
                    // If the problem to be solved is the enumerate extension problem, call the solver and write all
                    // of the labellings to columns in the node table
                    case EnumerateExtension:
                        List<Map<Object, ArgumentationSolverInterface.SolverLabelling>> labellings =
                                solver.solveEnumerateExtension(graph, semantics);
                        if (labellings == null || labellings.size() == 0) {
                            error = "No extension exists for these semantics.";
                        } else {
                            for (int i = 0; i < labellings.size(); i++) {
                                writeLabellingToColumn(semantics.name() + "_" + i, graph, labellings.get(i));
                            }
                        }
                        break;
                    // If the problem to be solved is an decision problem, then call the solver and write acceptance
                    // to the column
                    case DecideCredulous:
                        writeAcceptanceToColumn(semantics.name() + "_Credulous_Acceptance", graph,
                                solver.solveAcceptance(graph, semantics, true));
                        break;
                    case DecideSceptical:
                        writeAcceptanceToColumn(semantics.name() + "_Sceptical_Acceptance", graph,
                                solver.solveAcceptance(graph, semantics, false));
                        break;
                    case DecideCredulousViaExtensions:
                        writeAcceptanceToColumn(semantics.name() + "_Credulous_Acceptance", graph,
                                solver.solveAcceptanceViaExtensions(graph, semantics, true));
                        break;
                    case DecideScepticalViaExtensions:
                        writeAcceptanceToColumn(semantics.name() + "_Sceptical_Acceptance", graph,
                                solver.solveAcceptanceViaExtensions(graph, semantics, false));
                        break;
                }
                computed = true;
            } catch (IOException e) {
                e.printStackTrace();
                // If there is an exception, save it so it can be handled later
                executionException.set(e);
            }
        });
        System.out.println("Setup thread");
        try {
            t.start();
            t.join();
            if (executionException.get() != null) throw executionException.get();
        } catch (Throwable e) {
            error = e.getMessage();
        }

    }

    public ArgumentationSolverInterface getSolver() {
        return solver;
    }

    public void setSolver(ArgumentationSolverInterface solver) {
        this.solver = solver;
    }

    private void writeAcceptanceToColumn(String columnName, DirectedGraph graph, Map<Object,
            ArgumentationSolverInterface.AcceptanceLabelling> labellingMap) {
        graph.readLock();
        // Add column if it doesn't exist
        Table nodeTable = graph.getModel().getNodeTable();
        Column labellingColumn = nodeTable.getColumn(columnName);
        if (labellingColumn == null) {
            labellingColumn = nodeTable.addColumn(columnName, String.class);
        }
        // Write values to column
        for (Node node : graph.getNodes().toArray()) {
            node.setAttribute(labellingColumn, labellingMap.get(node.getId().toString()).toString());
        }
        graph.readUnlockAll();
    }

    private void writeLabellingToColumn(String columnName, DirectedGraph graph, Map<Object,
            ArgumentationSolverInterface.SolverLabelling> labellingMap) {
        graph.readLock();
        // Add column if it doesn't exist
        Table nodeTable = graph.getModel().getNodeTable();
        Column labellingColumn = nodeTable.getColumn(columnName);
        if (labellingColumn == null) {
            labellingColumn = nodeTable.addColumn(columnName, String.class);
        }
        // Write values to column
        for (Node node : graph.getNodes()) {
            node.setAttribute(labellingColumn, labellingMap.get(node.getId().toString()).toString());
        }
        graph.readUnlockAll();

    }

    /**
     * Gets the report generated by the solver
     *
     * @return A HTML string containing the report
     */
    @Override
    public String getReport() {
        String htmlString = "<html><body>";

        if (!error.isEmpty()) {
            htmlString += ("<h1>Argumentation Solver Error</h1><p>" + error + "</p>");
        } else {
            htmlString += "<h1>Argumentation Solver Completed</h1>";
        }

        htmlString += "</body></html>";

        return htmlString;
    }
}
