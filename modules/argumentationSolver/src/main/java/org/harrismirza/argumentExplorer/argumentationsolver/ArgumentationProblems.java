package org.harrismirza.argumentExplorer.argumentationsolver;

public enum ArgumentationProblems {
    DecideCredulous("DC"),
    DecideSceptical("DS"),
    SomeExtension("SE"),
    EnumerateExtension("EE"),
    DecideCredulousViaExtensions("DC(EE)"),
    DecideScepticalViaExtensions("DS(EE)");

    private String abbreviation;

    ArgumentationProblems(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public static ArgumentationProblems getProblemFromAbbreviation(String abbreviation) {
        for (ArgumentationProblems value : values()) {
            if (value.abbreviation.equalsIgnoreCase(abbreviation)) {
                return value;
            }
        }
        return null;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
