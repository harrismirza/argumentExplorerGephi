package org.harrismirza.argumentExplorer.argumentationsolver;

import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.openide.util.Lookup;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ArgumentationSolverTest {

    private static Logger log = Logger.getAnonymousLogger();
    private GraphModel graphModel;


    @BeforeEach
    public void setUpWorkspace() {
        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        projectController.newProject();
        Workspace workspace = projectController.newWorkspace(projectController.getCurrentProject());
        projectController.openWorkspace(workspace);
        graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);
        Graph graph = graphModel.getGraph();

        //Create a graph
        GraphFactory graphFactory = graphModel.factory();

        Node node0 = graphFactory.newNode("0");
        Node node1 = graphFactory.newNode("1");
        Node node2 = graphFactory.newNode("2");
        Node node3 = graphFactory.newNode("3");
        Node node4 = graphFactory.newNode("4");
        Node node5 = graphFactory.newNode("5");
        Node node6 = graphFactory.newNode("6");
        Node node7 = graphFactory.newNode("7");
        Node node8 = graphFactory.newNode("8");

        Edge edge01 = graphFactory.newEdge(node0, node1);
        Edge edge12 = graphFactory.newEdge(node1, node2);
        Edge edge20 = graphFactory.newEdge(node2, node0);
        Edge edge23 = graphFactory.newEdge(node2, node3);
        Edge edge31 = graphFactory.newEdge(node3, node1);
        Edge edge43 = graphFactory.newEdge(node4, node3);
        Edge edge56 = graphFactory.newEdge(node5, node6);
        Edge edge64 = graphFactory.newEdge(node6, node4);
        Edge edge75 = graphFactory.newEdge(node7, node5);
        Edge edge78 = graphFactory.newEdge(node7, node8);
        Edge edge85 = graphFactory.newEdge(node8, node5);
        Edge edge87 = graphFactory.newEdge(node8, node7);

        graph.addNode(node0);
        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node4);
        graph.addNode(node5);
        graph.addNode(node6);
        graph.addNode(node7);
        graph.addNode(node8);

        graph.addEdge(edge01);
        graph.addEdge(edge12);
        graph.addEdge(edge20);
        graph.addEdge(edge23);
        graph.addEdge(edge31);
        graph.addEdge(edge43);
        graph.addEdge(edge56);
        graph.addEdge(edge64);
        graph.addEdge(edge75);
        graph.addEdge(edge78);
        graph.addEdge(edge85);
        graph.addEdge(edge87);

    }

    private ArgumentationSolverStatistic executeSolverExtensionParsing(String problem, String solverProblem,
                                                                       String solverOutput)
            throws IOException, InterruptedException {
        ArgumentationSolverStatisticBuilder argSolverBuilder = new ArgumentationSolverStatisticBuilder();
        ArgumentationSolverStatistic argSolver = ((ArgumentationSolverStatistic) argSolverBuilder.getStatistics());

        ArgumentationSolverInterface mockedInterface = Mockito.spy(new ExecutableArgumentationSolverInterface("./test"));
        doReturn("").when(mockedInterface).callSolver(false,"");
        doReturn("[" + solverProblem + "]").when(mockedInterface).callSolver(true,"--problems");
        doReturn("[tgf]").when(mockedInterface).callSolver(true,"--formats");
        doReturn(solverOutput).when(mockedInterface).callSolver(true, "-p", solverProblem, "-f", "/temp.tgf", "-fo", "tgf");

        argSolver.setProblem(problem);
        argSolver.setSolver(mockedInterface);

        argSolver.execute(graphModel);

        while (!argSolver.computed) {
            Thread.sleep(100);
        }

        return argSolver;
    }


    private ArgumentationSolverStatistic executeSolverDecisionParsing(String problem, Map<String, String> solverOutputMap) throws IOException, InterruptedException {
        ArgumentationSolverStatisticBuilder argSolverBuilder = new ArgumentationSolverStatisticBuilder();
        ArgumentationSolverStatistic argSolver = ((ArgumentationSolverStatistic) argSolverBuilder.getStatistics());

        ArgumentationSolverInterface mockedInterface = Mockito.spy(new ExecutableArgumentationSolverInterface("./test"));
        doReturn("").when(mockedInterface).callSolver(false,"");
        doReturn("[" + problem + "]").when(mockedInterface).callSolver(false,"--problems");
        doReturn("[tgf]").when(mockedInterface).callSolver(false,"--formats");
        for (Map.Entry<String, String> entry : solverOutputMap.entrySet()) {
            String argument = entry.getKey();
            String output = entry.getValue();
            doReturn(output).when(mockedInterface).callSolver(true, "-p", problem, "-f", "/temp.tgf", "-fo", "tgf", "-a", argument);
        }


        argSolver.setProblem(problem);
        argSolver.setSolver(mockedInterface);

        argSolver.execute(graphModel);

        while (!argSolver.computed) {
            Thread.sleep(100);
        }

        return argSolver;
    }

    @Test
    public void testSolverQuery() throws IOException, InterruptedException {
        ArgumentationSolverInterface mockedInterface = Mockito.spy(new ExecutableArgumentationSolverInterface("./test"));
        doReturn("This is the solver details").when(mockedInterface).callSolver(false);
        doReturn("[AA-BB,DS-CO, 53-42,TEST,EE-ID]").when(mockedInterface).callSolver(true,"--problems");
        doReturn("[tgf,apx, TEST]").when(mockedInterface).callSolver(true,"--formats");

        mockedInterface.querySolverInfo();

        assert(mockedInterface.getSupportedFormats().containsAll(Arrays.asList("tgf","apx","TEST")));
        assert(mockedInterface.getSupportedProblems().containsAll(Arrays.asList("DS-CO","EE-ID","DS(EE)-ID","DC(EE)-ID")));
        assert(mockedInterface.getSolverInfo().equals("This is the solver details"));

    }

    @Test
    public void testSomeExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("SE-CO", "SE-CO", "[1,2,3]");

        assertEquals(ArgumentationSolverInterface.SolverLabelling.OUT.toString(),
                graphModel.getGraph().getNode("0").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("1").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("2").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("3").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("4").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("5").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("6").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("7").getAttribute("complete"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("8").getAttribute("complete"));
    }

    @Test
    public void testEnumerateExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("EE-CO", "EE-CO", "[[1,2,3][2,3,4,5,6]]");

        assertEquals(ArgumentationSolverInterface.SolverLabelling.OUT.toString(),
                graphModel.getGraph().getNode("0").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("1").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("2").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("3").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("4").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("5").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("6").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("7").getAttribute("complete_0"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("8").getAttribute("complete_0"));

        assertEquals(ArgumentationSolverInterface.SolverLabelling.OUT.toString(),
                graphModel.getGraph().getNode("0").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.OUT.toString(),
                graphModel.getGraph().getNode("1").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("2").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("3").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("4").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("5").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.IN.toString(),
                graphModel.getGraph().getNode("6").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("7").getAttribute("complete_1"));
        assertEquals(ArgumentationSolverInterface.SolverLabelling.UNDEC.toString(),
                graphModel.getGraph().getNode("8").getAttribute("complete_1"));
    }

    @Test
    public void testSomeExtensionsWithNoExtensions() throws IOException, InterruptedException {
        ArgumentationSolverStatistic argSolver = executeSolverExtensionParsing("SE-CO", "SE-CO", "NO");

        int numCols = graphModel.getNodeTable().countColumns();
        boolean anyComplete =
                IntStream.range(0, numCols).mapToObj(i -> graphModel.getNodeTable().getColumn(i)).anyMatch(column -> column.getId().contains("complete"));

        assert (!anyComplete);
        assert (argSolver.getReport().contains("No extension exists for these semantics."));
    }

    @Test
    public void testEnumerateExtensionsWithNoExtensions() throws IOException, InterruptedException {
        ArgumentationSolverStatistic argSolver = executeSolverExtensionParsing("EE-CO", "EE-CO", "[]");

        int numCols = graphModel.getNodeTable().countColumns();
        boolean anyComplete =
                IntStream.range(0, numCols).mapToObj(i -> graphModel.getNodeTable().getColumn(i)).anyMatch(column -> column.getId().contains("complete"));

        assert (!anyComplete);
        assert (argSolver.getReport().contains("No extension exists for these semantics."));
    }

    @Test
    public void testDecideCredulousViaExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("DC(EE)-CO", "EE-CO", "[[1,2,3][2,3,4,5,6]]");

        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("0"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "1").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "2").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "3").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "4").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "5").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "6").getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("7"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("8"
        ).getAttribute("complete_credulous_acceptance"));

    }

    @Test
    public void testDecideScepticalViaExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("DS(EE)-CO", "EE-CO", "[[1,2,3],[2,3,4,5,6]]");

        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("0"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("1"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "2").getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.YES.toString(), graphModel.getGraph().getNode(
                "3").getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("4"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("5"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("6"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("7"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("8"
        ).getAttribute("complete_sceptical_acceptance"));

    }

    @Test
    public void testDecideCredulousViaExtensionsWithNoExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("DC(EE)-CO", "EE-CO", "[]");

        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("0"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("1"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("2"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("3"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("4"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("5"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("6"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("7"
        ).getAttribute("complete_credulous_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("8"
        ).getAttribute("complete_credulous_acceptance"));

    }

    @Test
    public void testDecideScepticalViaExtensionsWithNoExtensions() throws IOException, InterruptedException {
        executeSolverExtensionParsing("DS(EE)-CO", "EE-CO", "[]");

        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("0"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("1"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("2"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("3"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("4"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("5"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("6"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("7"
        ).getAttribute("complete_sceptical_acceptance"));
        assertEquals(ArgumentationSolverInterface.AcceptanceLabelling.NO.toString(), graphModel.getGraph().getNode("8"
        ).getAttribute("complete_sceptical_acceptance"));

    }

    @Test
    public void testDecideCredulous() throws IOException, InterruptedException {
        Map<String, String> credulousAcceptance = new HashMap<String, String>(){{
            put("0", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("1", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("2", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("3", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("4", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("5", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("6", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("7", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("8", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
        }};

        executeSolverDecisionParsing("DC-CO", credulousAcceptance);

        credulousAcceptance.forEach((argument, output) -> {
            assertEquals(output, graphModel.getGraph().getNode(argument).getAttribute("complete_credulous_acceptance"));
        });
    }

    @Test
    public void testDecideSceptical() throws IOException, InterruptedException {
        Map<String, String> scepticalAcceptance = new HashMap<String, String>(){{
            put("0", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("1", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("2", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("3", ArgumentationSolverInterface.AcceptanceLabelling.YES.toString());
            put("4", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("5", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("6", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("7", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
            put("8", ArgumentationSolverInterface.AcceptanceLabelling.NO.toString());
        }};

        executeSolverDecisionParsing("DS-CO", scepticalAcceptance);

        scepticalAcceptance.forEach((argument, output) -> {
            assertEquals(output, graphModel.getGraph().getNode(argument).getAttribute("complete_sceptical_acceptance"));
        });
    }


    @Test
    public void testDockerImagesParsing() throws IOException {
        String testOutput =
                "REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE\n" +
                "harrismirza/python_pandas   3.8                 c81b15ff08d2        2 weeks ago         1.07GB\n" +
                "andreasniskanen/iccma2019   latest              2c32f1152735        12 months ago       10.7MB\n" +
                "odinaldo/eqargsolver        latest              4775dedcf317        13 months ago       13.7MB";

        List<DockerImageMetadata> expectedMetadata = new ArrayList<>();
        expectedMetadata.add(new DockerImageMetadata("harrismirza/python_pandas", "3.8", "c81b15ff08d2", "2 weeks ago", "1.07GB"));
        expectedMetadata.add(new DockerImageMetadata("andreasniskanen/iccma2019", "latest", "2c32f1152735", "12 months ago", "10.7MB"));
        expectedMetadata.add(new DockerImageMetadata("odinaldo/eqargsolver", "latest", "4775dedcf317", "13 months ago", "13.7MB"));

        List<DockerImageMetadata> imageMetadata = DockerImagesInterface.parseDockerImagesOutput(Arrays.asList(testOutput.split("\n")));

        for (int i = 0; i < 3; i++) {
            DockerImageMetadata a = imageMetadata.get(i);
            DockerImageMetadata e = expectedMetadata.get(i);
            log.info("" + a.repository.equals(e.repository));
            log.info("" + a.tag.equals(e.tag));
            log.info("" + a.imageId.equals(e.imageId));
            log.info("" + a.created.equals(e.created));
            log.info("" + a.size.equals(e.size));
        }

        assertEquals(imageMetadata, expectedMetadata);
    }

}