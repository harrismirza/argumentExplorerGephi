package org.harrismirza.argumentExplorer.frameworkimporter;

import org.gephi.io.importer.api.ElementDraft;
import org.gephi.io.importer.api.Report;
import org.gephi.io.importer.impl.ImportContainerImpl;
import org.gephi.io.importer.spi.FileImporter;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class APXImporterTest {

    private static Logger log = Logger.getAnonymousLogger();

    @BeforeEach
    public void setUpWorkspace() {
        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        projectController.newProject();
        Workspace workspace = projectController.newWorkspace(projectController.getCurrentProject());
        projectController.openWorkspace(workspace);
    }

    @Test
    public void testImporter() throws URISyntaxException, FileNotFoundException {
        APXImporterBuilder apxImporterBuilder = new APXImporterBuilder();
        FileImporter apxImporter = apxImporterBuilder.buildImporter();
        File file = new File(getClass().getResource("/org/harrismirza/argumentExplorer/normalInput.apx").getPath());
        apxImporter.setReader((new FileReader(file)));

        ImportContainerImpl containerLoader = new ImportContainerImpl();
        containerLoader.setReport(new Report());

        apxImporter.execute(containerLoader);


        List<String> actualNodeNames =
                StreamSupport.stream(containerLoader.getNodes().spliterator(), false).map(ElementDraft::getId).collect(Collectors.toList());
        List<String> expectedNodeNames = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8");

        List<Map.Entry<String, String>> actualEdges = StreamSupport.stream(containerLoader.getEdges().spliterator(),
                false).map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId(), edge.getTarget().getId())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("5", "6"),
                new AbstractMap.SimpleEntry<>("6", "4"),
                new AbstractMap.SimpleEntry<>("7", "5"),
                new AbstractMap.SimpleEntry<>("7", "8"),
                new AbstractMap.SimpleEntry<>("8", "5"),
                new AbstractMap.SimpleEntry<>("8", "7")
        );


        assert (actualNodeNames.containsAll(expectedNodeNames) && expectedNodeNames.containsAll(actualNodeNames));
        assert (actualEdges.containsAll(expectedEdges) && expectedEdges.containsAll(actualEdges));

    }
}
