package org.harrismirza.argumentExplorer.frameworkimporter;

import org.gephi.io.importer.api.*;
import org.gephi.io.importer.spi.FileImporter;

import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that imports Aspartix format graphs
 */
public class APXImporter implements FileImporter {

    private Reader reader;
    private ContainerLoader container;
    private Report report;

    @Override
    public void setReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public boolean execute(ContainerLoader containerLoader) {
        this.container = containerLoader;
        this.report = new Report();
        try {
            LineNumberReader lineNumberReader = ImportUtils.getTextReader(reader);
            container.setEdgeDefault(EdgeDirectionDefault.DIRECTED);
            container.setAllowSelfLoop(true);
            container.setAllowParallelEdge(false);

            // Read File
            List<String> nodeIds = new ArrayList<>();
            List<String[]> edges = new ArrayList<>();
            String line;
            while ((line = lineNumberReader.readLine()) != null) {
                line = line.trim();
                if (line.length() == 0) {
                    continue;
                }
                report.log(line);
                if (line.startsWith("att")) {
                    // Find node id
                    int openBracketIndex = line.indexOf("(");
                    int closedBracketIndex = line.indexOf(")");
                    line = line.substring(openBracketIndex + 1, closedBracketIndex);
                    report.log("Edge: " + line);
                    String[] edgeNodes = line.split(",");
                    edges.add(edgeNodes);
                } else if (line.startsWith("arg")) {
                    int openBracketIndex = line.indexOf("(");
                    int closedBracketIndex = line.indexOf(")");
                    String nodeId = line.substring(openBracketIndex + 1, closedBracketIndex);
                    report.log("Node: " + nodeId);
                    nodeIds.add(nodeId);
                }
            }

            //Create nodes and edges
            for (String nodeId : nodeIds) {
                NodeDraft node = container.factory().newNodeDraft(nodeId);
                container.addNode(node);
            }

            for (String[] edgeIds : edges) {
                EdgeDraft edge = container.factory().newEdgeDraft();
                edge.setSource(container.getNode(edgeIds[0]));
                edge.setTarget(container.getNode(edgeIds[1]));
                container.addEdge(edge);
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    @Override
    public ContainerLoader getContainer() {
        return container;
    }

    @Override
    public Report getReport() {
        return report;
    }
}
