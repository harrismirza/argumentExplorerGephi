package org.harrismirza.argumentExplorer.frameworkimporter;

import org.gephi.io.importer.api.*;
import org.gephi.io.importer.spi.FileImporter;

import java.io.LineNumberReader;
import java.io.Reader;

/**
 * Class that imports Trivial Graph Format graphs
 */
public class TGFImporter implements FileImporter {

    private Reader reader;
    private ContainerLoader container;
    private Report report;

    @Override
    public void setReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public boolean execute(ContainerLoader containerLoader) {
        this.container = containerLoader;
        this.report = new Report();
        try {
            LineNumberReader lineNumberReader = ImportUtils.getTextReader(reader);
            container.setEdgeDefault(EdgeDirectionDefault.DIRECTED);
            container.setAllowSelfLoop(true);
            container.setAllowParallelEdge(false);

            // Read File
            boolean isEdge = false;
            String line;
            while ((line = lineNumberReader.readLine()) != null) {
                line = line.trim();
                if (line.length() == 0) {
                    continue;
                }
                if (isEdge) {
                    String[] nodeIds = line.split(" ");
                    String node1Id = nodeIds[0];
                    String node2Id = nodeIds[1];
                    EdgeDraft edge = container.factory().newEdgeDraft();
                    edge.setSource(container.getNode(node1Id));
                    edge.setTarget(container.getNode(node2Id));
                    container.addEdge(edge);
                } else if (line.equals("#")) {
                    isEdge = true;
                } else {
                    NodeDraft node = container.factory().newNodeDraft(line);
                    container.addNode(node);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    @Override
    public ContainerLoader getContainer() {
        return container;
    }

    @Override
    public Report getReport() {
        return report;
    }
}
