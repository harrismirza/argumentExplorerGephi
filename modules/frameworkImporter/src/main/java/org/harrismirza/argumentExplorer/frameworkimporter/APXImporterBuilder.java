package org.harrismirza.argumentExplorer.frameworkimporter;

import org.gephi.io.importer.api.FileType;
import org.gephi.io.importer.spi.FileImporter;
import org.gephi.io.importer.spi.FileImporterBuilder;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.ServiceProvider;

/**
 * Builder class that creates instances of the {@link org.harrismirza.argumentExplorer.frameworkimporter.APXImporter} class
 */
@ServiceProvider(service = FileImporterBuilder.class)
public class APXImporterBuilder implements FileImporterBuilder {
    private static final String IDENTIFIER = "apx";

    @Override
    public FileImporter buildImporter() {
        return new APXImporter();
    }

    @Override
    public String getName() {
        return IDENTIFIER;
    }

    @Override
    public FileType[] getFileTypes() {
        return new FileType[]{new FileType("." + IDENTIFIER, "Aspartix Graph Format")};
    }

    @Override
    public boolean isMatchingImporter(FileObject fileObject) {
        return fileObject.getExt().equalsIgnoreCase(IDENTIFIER);
    }
}
