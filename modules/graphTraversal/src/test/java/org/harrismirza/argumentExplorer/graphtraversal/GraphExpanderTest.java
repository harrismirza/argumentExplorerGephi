package org.harrismirza.argumentExplorer.graphtraversal;

import org.gephi.filters.FilterControllerImpl;
import org.gephi.filters.api.FilterController;
import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.tools.spi.NodeClickEventListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openide.util.Lookup;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class GraphExpanderTest {
    private static Logger log = Logger.getAnonymousLogger();
    private ProjectController projectController;
    private FilterController filterController;
    private ExpandContractGraphToolController expandContractGraphToolController;
    private GraphModel graphModel;
    private Workspace workspace;

    @BeforeEach
    public void setUpWorkspace() {
        projectController = Lookup.getDefault().lookup(ProjectController.class);
        projectController.newProject();

        workspace = projectController.newWorkspace(projectController.getCurrentProject());
        projectController.openWorkspace(workspace);
        graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);
        Graph graph = graphModel.getGraph();

        expandContractGraphToolController = Lookup.getDefault().lookup(ExpandContractGraphToolController.class);

        filterController = new FilterControllerImpl();

        //Create a graph
        GraphFactory graphFactory = graphModel.factory();

        Node node0 = graphFactory.newNode("0");
        Node node1 = graphFactory.newNode("1");
        Node node2 = graphFactory.newNode("2");
        Node node3 = graphFactory.newNode("3");
        Node node4 = graphFactory.newNode("4");
        Node node5 = graphFactory.newNode("5");
        Node node6 = graphFactory.newNode("6");
        Node node7 = graphFactory.newNode("7");
        Node node8 = graphFactory.newNode("8");

        Edge edge01 = graphFactory.newEdge(node0, node1);
        Edge edge12 = graphFactory.newEdge(node1, node2);
        Edge edge20 = graphFactory.newEdge(node2, node0);
        Edge edge23 = graphFactory.newEdge(node2, node3);
        Edge edge31 = graphFactory.newEdge(node3, node1);
        Edge edge43 = graphFactory.newEdge(node4, node3);
        Edge edge56 = graphFactory.newEdge(node5, node6);
        Edge edge64 = graphFactory.newEdge(node6, node4);
        Edge edge75 = graphFactory.newEdge(node7, node5);
        Edge edge78 = graphFactory.newEdge(node7, node8);
        Edge edge85 = graphFactory.newEdge(node8, node5);
        Edge edge87 = graphFactory.newEdge(node8, node7);

        graph.addNode(node0);
        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node4);
        graph.addNode(node5);
        graph.addNode(node6);
        graph.addNode(node7);
        graph.addNode(node8);

        graph.addEdge(edge01);
        graph.addEdge(edge12);
        graph.addEdge(edge20);
        graph.addEdge(edge23);
        graph.addEdge(edge31);
        graph.addEdge(edge43);
        graph.addEdge(edge56);
        graph.addEdge(edge64);
        graph.addEdge(edge75);
        graph.addEdge(edge78);
        graph.addEdge(edge85);
        graph.addEdge(edge87);

        expandContractGraphToolController.reset(workspace.getId());

    }

    @Test
    public void testContractThenExpandAllEdges() {
        ExpandContractGraphTool expandContractGraphTool = new ExpandContractGraphTool();
        expandContractGraphTool.select();
        NodeClickEventListener nodeListener = (NodeClickEventListener) expandContractGraphTool.getListeners()[0];

        //Contract Node and test nodes and edges in visible graph
        expandContractGraphTool.contractMode = true;
        Node node5 = graphModel.getGraph().getNode("5");

        nodeListener.clickNodes(new Node[]{node5});

        GraphView filterView1 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView1);

        List<String> actualNodeNames1 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames1 = Arrays.asList("0", "1", "2", "3", "4", "5");

        List<Map.Entry<String, String>> actualEdges1 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges1 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3")
        );

        assert (actualNodeNames1.containsAll(expectedNodeNames1) && expectedNodeNames1.containsAll(actualNodeNames1));
        assert (actualEdges1.containsAll(expectedEdges1) && expectedEdges1.containsAll(actualEdges1));

        //Contract other node
        Node node0 = graphModel.getGraph().getNode("0");

        nodeListener.clickNodes(new Node[]{node0});

        GraphView filterView2 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView2);

        List<String> actualNodeNames2 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames2 = Arrays.asList("0", "3", "4", "5");

        List<Map.Entry<String, String>> actualEdges2 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges2 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("4", "3")
        );

        assert (actualNodeNames2.containsAll(expectedNodeNames2) && expectedNodeNames2.containsAll(actualNodeNames2));
        assert (actualEdges2.containsAll(expectedEdges2) && expectedEdges2.containsAll(actualEdges2));

        //Expand other node
        expandContractGraphTool.contractMode = false;
        Node node3 = graphModel.getGraph().getNode("3");

        nodeListener.clickNodes(new Node[]{node3});

        GraphView filterView3 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView3);

        List<String> actualNodeNames3 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames3 = Arrays.asList("0", "1", "2", "3", "4", "5");

        List<Map.Entry<String, String>> actualEdges3 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges3 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3")
        );

        assert (actualNodeNames3.containsAll(expectedNodeNames3) && expectedNodeNames3.containsAll(actualNodeNames3));
        assert (actualEdges3.containsAll(expectedEdges3) && expectedEdges3.containsAll(actualEdges3));

    }

    @Test
    public void testContractThenExpandInEdges() {
        ExpandContractGraphTool expandContractGraphTool = new ExpandContractGraphTool();
        expandContractGraphTool.select();
        expandContractGraphTool.followOutEdges = false;
        NodeClickEventListener nodeListener = (NodeClickEventListener) expandContractGraphTool.getListeners()[0];

        //Contract Node and test nodes and edges in visible graph
        expandContractGraphTool.contractMode = true;
        Node node5 = graphModel.getGraph().getNode("5");
        nodeListener.clickNodes(new Node[]{node5});

        GraphView filterView1 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView1);

        List<String> actualNodeNames1 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames1 = Arrays.asList("0", "1", "2", "3", "4", "5", "6");

        List<Map.Entry<String, String>> actualEdges1 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges1 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("5", "6"),
                new AbstractMap.SimpleEntry<>("6", "4")
        );

        assert (actualNodeNames1.containsAll(expectedNodeNames1) && expectedNodeNames1.containsAll(actualNodeNames1));
        assert (actualEdges1.containsAll(expectedEdges1) && expectedEdges1.containsAll(actualEdges1));

        //Contract other node
        Node node0 = graphModel.getGraph().getNode("0");

        nodeListener.clickNodes(new Node[]{node0});

        GraphView filterView2 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView2);

        List<String> actualNodeNames2 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames2 = Arrays.asList("0", "1", "3", "4", "5", "6");

        List<Map.Entry<String, String>> actualEdges2 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges2 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("5", "6"),
                new AbstractMap.SimpleEntry<>("6", "4")
        );

        assert (actualNodeNames2.containsAll(expectedNodeNames2) && expectedNodeNames2.containsAll(actualNodeNames2));
        assert (actualEdges2.containsAll(expectedEdges2) && expectedEdges2.containsAll(actualEdges2));

        //Expand other node
        expandContractGraphTool.contractMode = false;
        Node node3 = graphModel.getGraph().getNode("3");

        nodeListener.clickNodes(new Node[]{node3});

        GraphView filterView3 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView3);

        List<String> actualNodeNames3 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames3 = Arrays.asList("0", "1", "2", "3", "4", "5", "6");

        List<Map.Entry<String, String>> actualEdges3 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges3 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("5", "6"),
                new AbstractMap.SimpleEntry<>("6", "4")
        );

        assert (actualNodeNames3.containsAll(expectedNodeNames3) && expectedNodeNames3.containsAll(actualNodeNames3));
        assert (actualEdges3.containsAll(expectedEdges3) && expectedEdges3.containsAll(actualEdges3));

    }

    @Test
    public void testContractThenExpandOutEdges() {
        ExpandContractGraphTool expandContractGraphTool = new ExpandContractGraphTool();
        expandContractGraphTool.select();
        expandContractGraphTool.followInEdges = false;
        NodeClickEventListener nodeListener = (NodeClickEventListener) expandContractGraphTool.getListeners()[0];

        //Contract Node and test nodes and edges in visible graph
        expandContractGraphTool.contractMode = true;
        Node node5 = graphModel.getGraph().getNode("5");

        nodeListener.clickNodes(new Node[]{node5});

        GraphView filterView1 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView1);

        List<String> actualNodeNames1 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames1 = Arrays.asList("0", "1", "2", "3", "4", "5", "7", "8");

        List<Map.Entry<String, String>> actualEdges1 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges1 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("7", "5"),
                new AbstractMap.SimpleEntry<>("7", "8"),
                new AbstractMap.SimpleEntry<>("8", "5"),
                new AbstractMap.SimpleEntry<>("8", "7")
        );

        assert (actualNodeNames1.containsAll(expectedNodeNames1) && expectedNodeNames1.containsAll(actualNodeNames1));
        assert (actualEdges1.containsAll(expectedEdges1) && expectedEdges1.containsAll(actualEdges1));

        //Contract other node
        Node node0 = graphModel.getGraph().getNode("0");

        nodeListener.clickNodes(new Node[]{node0});

        GraphView filterView2 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView2);

        List<String> actualNodeNames2 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames2 = Arrays.asList("0", "2", "3", "4", "5", "7", "8");

        List<Map.Entry<String, String>> actualEdges2 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges2 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("7", "5"),
                new AbstractMap.SimpleEntry<>("7", "8"),
                new AbstractMap.SimpleEntry<>("8", "5"),
                new AbstractMap.SimpleEntry<>("8", "7")
        );

        assert (actualNodeNames2.containsAll(expectedNodeNames2) && expectedNodeNames2.containsAll(actualNodeNames2));
        assert (actualEdges2.containsAll(expectedEdges2) && expectedEdges2.containsAll(actualEdges2));

        //Expand other node
        expandContractGraphTool.contractMode = false;
        Node node3 = graphModel.getGraph().getNode("3");

        nodeListener.clickNodes(new Node[]{node3});

        GraphView filterView3 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView3);

        List<String> actualNodeNames3 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames3 = Arrays.asList("0", "1", "2", "3", "4", "5", "7", "8");

        List<Map.Entry<String, String>> actualEdges3 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges3 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3"),
                new AbstractMap.SimpleEntry<>("7", "5"),
                new AbstractMap.SimpleEntry<>("7", "8"),
                new AbstractMap.SimpleEntry<>("8", "5"),
                new AbstractMap.SimpleEntry<>("8", "7")
        );

        assert (actualNodeNames3.containsAll(expectedNodeNames3) && expectedNodeNames3.containsAll(actualNodeNames3));
        assert (actualEdges3.containsAll(expectedEdges3) && expectedEdges3.containsAll(actualEdges3));

    }

    @Test
    public void testContractAll() {
        ExpandContractGraphTool expandContractGraphTool = new ExpandContractGraphTool();
        expandContractGraphTool.select();
        expandContractGraphTool.followInEdges = false;
        NodeClickEventListener nodeListener = (NodeClickEventListener) expandContractGraphTool.getListeners()[0];

        //Contract all nodes
        expandContractGraphTool.contractAllMode = true;

        Node node5 = graphModel.getGraph().getNode("5");
        nodeListener.clickNodes(new Node[]{node5});

        GraphView filterView1 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView1);

        List<String> actualNodeNames1 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames1 = Collections.singletonList("5");

        List<Map.Entry<String, String>> actualEdges1 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges1 = Collections.emptyList();

        assert (actualNodeNames1.containsAll(expectedNodeNames1) && expectedNodeNames1.containsAll(actualNodeNames1));
        assert (actualEdges1.containsAll(expectedEdges1) && expectedEdges1.containsAll(actualEdges1));
    }

    @Test
    public void testLargestComponent() {
        ExpandContractGraphTool expandContractGraphTool = new ExpandContractGraphTool();
        expandContractGraphTool.select();
        expandContractGraphTool.followInEdges = false;
        NodeClickEventListener nodeListener = (NodeClickEventListener) expandContractGraphTool.getListeners()[0];

        //Contract Node and test nodes and edges in visible graph
        expandContractGraphTool.contractMode = true;
        Node node5 = graphModel.getGraph().getNode("5");

        nodeListener.clickNodes(new Node[]{node5});

        GraphView filterView1 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView1);

        expandContractGraphToolController.keepLargestComponent(workspace.getId());

        GraphView filterView2 =
                filterController.filter(expandContractGraphToolController.currentQueries.get(projectController.getCurrentWorkspace().getId()));
        graphModel.setVisibleView(filterView2);

        List<String> actualNodeNames1 =
                graphModel.getGraphVisible().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        log.info(actualNodeNames1.toString());
        List<String> expectedNodeNames1 = Arrays.asList("0", "1", "2", "3", "4");

        List<Map.Entry<String, String>> actualEdges1 =
                graphModel.getGraphVisible().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        log.info(actualEdges1.toString());
        List<Map.Entry<String, String>> expectedEdges1 = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("4", "3")
        );

        assert (actualNodeNames1.containsAll(expectedNodeNames1) && expectedNodeNames1.containsAll(actualNodeNames1));
        assert (actualEdges1.containsAll(expectedEdges1) && expectedEdges1.containsAll(actualEdges1));

    }


}
