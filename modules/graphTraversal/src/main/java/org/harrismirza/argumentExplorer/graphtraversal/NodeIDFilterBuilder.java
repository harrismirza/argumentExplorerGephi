package org.harrismirza.argumentExplorer.graphtraversal;

import org.gephi.filters.api.FilterLibrary;
import org.gephi.filters.spi.*;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.project.api.Workspace;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;
import java.util.Set;

/**
 * This class creates instances of a filter that filters nodes based on a set of ids
 */
@ServiceProvider(service = FilterBuilder.class)
public class NodeIDFilterBuilder implements FilterBuilder {
    private Set<Object> currentNodes;

    public NodeIDFilterBuilder() {
        super();
    }

    NodeIDFilterBuilder(Set<Object> currentNodes) {
        super();
        this.currentNodes = currentNodes;
    }

    @Override
    public Category getCategory() {
        return FilterLibrary.ATTRIBUTES;
    }

    @Override
    public String getName() {
        return "Node ID Filter";
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Filter by Node ID";
    }

    @Override
    public Filter getFilter(Workspace workspace) {
        return new NodeFilter() {
            Graph graph;

            public boolean init(Graph graph) {
                this.graph = graph;
                return true;
            }

            public boolean evaluate(Graph graph, Node node) {
                return currentNodes.contains(node.getId());
            }

            public void finish() {
            }

            public String getName() {
                return "Node ID Filter";
            }

            public FilterProperty[] getProperties() {
                return new FilterProperty[0];
            }
        };
    }

    @Override
    public JPanel getPanel(Filter filter) {
        return new JPanel();
    }

    @Override
    public void destroy(Filter filter) {

    }
}
