package org.harrismirza.argumentExplorer.graphtraversal;

import org.gephi.project.api.ProjectController;
import org.gephi.tools.spi.Tool;
import org.gephi.tools.spi.ToolUI;
import org.openide.util.Lookup;

import javax.swing.*;

/**
 * Controls the Option Bar for the Graph Expander plugin
 */
public class ExpandContractGraphToolUI implements ToolUI {

    private ExpandContractGraphTool parent;

    ExpandContractGraphToolUI(ExpandContractGraphTool parent) {
        this.parent = parent;
    }

    @Override
    public JPanel getPropertiesBar(final Tool tool) {
        JPanel propertiesBar = new JPanel();

        JComboBox<String> contractModeComboBox = new JComboBox<>(new String[]{"Contract", "Expand", "Contract All"});
        JLabel contractModeComboBoxLabel = new JLabel("Mode: ");
        // Update the edge mode based on the dropdown values
        contractModeComboBox.addActionListener(e -> {
            switch (contractModeComboBox.getSelectedIndex()) {
                case 0:
                    parent.contractMode = true;
                    parent.contractAllMode = false;
                    break;
                case 1:
                    parent.contractMode = false;
                    parent.contractAllMode = false;
                    break;
                case 2:
                    parent.contractAllMode = true;
                    break;
            }
        });
        parent.contractMode = true;
        parent.contractAllMode = false;

        JComboBox<String> edgeModeComboBox = new JComboBox<>(new String[]{"All Edges", "In Edges", "Out Edges"});
        JLabel edgeModeComboBoxLabel = new JLabel("Edge Mode: ");
        // Update the edge mode based on the dropdown values
        edgeModeComboBox.addActionListener(e -> {
            switch (edgeModeComboBox.getSelectedIndex()) {
                case 0:
                    parent.followInEdges = true;
                    parent.followOutEdges = true;
                    break;
                case 1:
                    parent.followInEdges = true;
                    parent.followOutEdges = false;
                    break;
                case 2:
                    parent.followInEdges = false;
                    parent.followOutEdges = true;
                    break;
            }
        });
        parent.followInEdges = true;
        parent.followOutEdges = true;

        JButton keepLargestComponentButton = new JButton("Keep Largest Component");
        keepLargestComponentButton.addActionListener(e -> {
            int currentWorkspace = Lookup.getDefault().lookup(ProjectController.class).getCurrentWorkspace().getId();
            Lookup.getDefault().lookup(ExpandContractGraphToolController.class).keepLargestComponent(currentWorkspace);
        });

        JButton resetButton = new JButton("Reset Filter");
        resetButton.addActionListener(e -> {
            int currentWorkspace = Lookup.getDefault().lookup(ProjectController.class).getCurrentWorkspace().getId();
            Lookup.getDefault().lookup(ExpandContractGraphToolController.class).reset(currentWorkspace);
        });

        propertiesBar.add(contractModeComboBoxLabel);
        propertiesBar.add(contractModeComboBox);
        propertiesBar.add(edgeModeComboBoxLabel);
        propertiesBar.add(edgeModeComboBox);
        propertiesBar.add(keepLargestComponentButton);
        propertiesBar.add(resetButton);


        return propertiesBar;
    }

    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/org/harrismirza/argumentExplorer/graphtraversal/plus.png"));
    }

    @Override
    public String getName() {
        return "Graph Expander";
    }

    @Override
    public String getDescription() {
        return "Graph Expander";
    }

    @Override
    public int getPosition() {
        return 10000;
    }
}
