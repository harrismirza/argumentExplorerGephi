package org.harrismirza.argumentExplorer.graphtraversal;

import org.gephi.project.api.ProjectController;
import org.gephi.tools.spi.*;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * Creates the tool for the Graph Expander plugin, controls the node click events
 */
@ServiceProvider(service = Tool.class)
public class ExpandContractGraphTool implements Tool {

    boolean contractAllMode = false;
    boolean contractMode = true;
    boolean followInEdges = true;
    boolean followOutEdges = true;
    private ExpandContractGraphToolUI ui = new ExpandContractGraphToolUI(this);
    private ProjectController projectController;
    private ExpandContractGraphToolController expandContractGraphToolController;
    private int currentWorkspace;

    public ExpandContractGraphTool() {
        super();
        // Get current nodes and add to list
        projectController = Lookup.getDefault().lookup(ProjectController.class);
        expandContractGraphToolController = Lookup.getDefault().lookup(ExpandContractGraphToolController.class);
    }

    @Override
    public void select() {
        currentWorkspace = projectController.getCurrentWorkspace().getId();
        expandContractGraphToolController.selectTool(currentWorkspace);
    }

    @Override
    public void unselect() {

    }

    @Override
    public ToolEventListener[] getListeners() {
        return new ToolEventListener[]{
                (NodeClickEventListener) nodes -> {
                    // On node click, trigger expansion/contraction
                    expandContractGraphToolController.expandContractNodes(nodes, currentWorkspace, contractAllMode,
                            contractMode, followInEdges, followOutEdges);
                }
        };
    }

    @Override
    public ToolUI getUI() {
        return ui;
    }

    @Override
    public ToolSelectionType getSelectionType() {
        return ToolSelectionType.SELECTION;
    }

}


