package org.harrismirza.argumentExplorer.graphtraversal;

import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.spi.FilterBuilder;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller class for the Graph Expander plugin, controls the current nodes that are filtered in each workspace
 */
@ServiceProvider(service = ExpandContractGraphToolController.class)
public class ExpandContractGraphToolController {
    Map<Integer, Set<Object>> currentNodes = new HashMap<>();
    Map<Integer, Query> currentQueries = new HashMap<>();
    private GraphController graphController = Lookup.getDefault().lookup(GraphController.class);

    /**
     * Finds the largest component and applies it as a filter
     * @param currentWorkspace The workspace to apply the operation on
     */
    void keepLargestComponent(int currentWorkspace) {
        // Find the largest component and update the query to show only those nodes
        currentNodes.put(currentWorkspace,
                getLargestComponent(graphController.getGraphModel().getUndirectedGraphVisible()));
        updateQuery(currentWorkspace);
    }

    private Set<Object> getLargestComponent(Graph currentGraph) {
        List<Set<Object>> components = (new WeaklyConnectedComponent(currentGraph)).getComponents();
        System.out.println(components);
        return components.stream().max(Comparator.comparingInt(Set::size)).get();
    }

    /**
     * Clears the filter from the provided workspace
     * @param currentWorkspace The workspace to apply the operation on
     */
    void reset(int currentWorkspace) {
        // Reset by adding all nodes back into the visible set
        Set<Object> nodeSet = new HashSet<>();
        for (Node node : graphController.getGraphModel().getGraph().getNodes()) {
            nodeSet.add(node.getId());
        }
        currentNodes.put(currentWorkspace, nodeSet);
        updateQuery(currentWorkspace);
    }

    /**
     * Applies the current filter to the workspace
     * @param currentWorkspace The workspace to apply the operation on
     */
    private void updateQuery(int currentWorkspace) {
        // Update the filter options and apply them
        FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
        FilterBuilder nodeIDFilterBuilder = new NodeIDFilterBuilder(currentNodes.get(currentWorkspace));
        Query currentQuery = filterController.createQuery(nodeIDFilterBuilder);
        currentQueries.put(currentWorkspace, currentQuery);
        filterController.filterVisible(currentQuery);
    }

    /**
     * Sets up the data structures the first time the tool is selected
     * @param currentWorkspace The workspace to apply the operation on
     */
    void selectTool(int currentWorkspace) {
        System.out.println("Selected");
        // When the tool is selected, if this workspace has no filter, then initialise the set of current nodes
        if (!currentNodes.containsKey(currentWorkspace)) {
            System.out.println("Adding set");
            Set<Object> currentVisibleNodes = new HashSet<>();
            for (Node node : graphController.getGraphModel().getGraphVisible().getNodes()) {
                currentVisibleNodes.add(node.getId());
            }
            currentNodes.put(currentWorkspace, currentVisibleNodes);
        }
        updateQuery(currentWorkspace);
        System.out.println(currentNodes.get(currentWorkspace));
    }

    /**
     * Expands/Contracts nodes
     * @param nodes The nodes to expand or contract
     * @param currentWorkspace The workspace to apply the operation on
     * @param contractAllMode Whether to contract all nodes except the selected
     * @param contractMode Whether to contract/expand the nodes
     * @param followInEdges Whether to treat incoming edges as neighbours
     * @param followOutEdges Whether to treat outgoing edges as neighbours
     */
    void expandContractNodes(Node[] nodes, int currentWorkspace, boolean contractAllMode, boolean contractMode,
                             boolean followInEdges, boolean followOutEdges) {
        System.out.println("***********************");
        System.out.println("Nodes " + Arrays.toString(nodes));
        System.out.println("Workspace " + currentWorkspace);
        System.out.println("Contract All " + contractAllMode);
        System.out.println("Contract " + contractMode);
        System.out.println("Out Edges " + followOutEdges);
        System.out.println("In Edges " + followInEdges);
        System.out.println("***********************");

        // If contract all mode is selected, then remove all nodes from the set except the selected nodes
        if (contractAllMode) {
            // Contract to have only one node
            Set<Object> currentNodeSet = currentNodes.get(currentWorkspace);
            currentNodeSet.clear();
            for (Node node : nodes) {
                currentNodeSet.add(node.getId());
            }
            currentNodes.put(currentWorkspace, currentNodeSet);
        } else {
            // Otherwise, for each node, update the set by adding or removing its neighbours, based on the edge modes
            DirectedGraph graph = graphController.getGraphModel().getDirectedGraph();
            for (Node node : nodes) {
                Collection<Node> successors = graph.getSuccessors(node).toCollection();
                Collection<Node> predecessors = graph.getPredecessors(node).toCollection();
                List<Node> neighbours = new ArrayList<>();
                if (followOutEdges) {
                    neighbours.addAll(successors);
                }
                if (followInEdges) {
                    neighbours.addAll(predecessors);
                }
                neighbours.remove(node);
                System.out.println("Successors " + successors.stream().map(node1 -> node1.getId().toString()).collect(Collectors.joining(",")));
                System.out.println("Predecessors " + predecessors.stream().map(node1 -> node1.getId().toString()).collect(Collectors.joining(",")));
                System.out.println("Neighbours " + neighbours.stream().map(node1 -> node1.getId().toString()).collect(Collectors.joining(",")));
                Set<Object> currentNodeSet = currentNodes.get(currentWorkspace);
                for (Node neighbour : neighbours) {
                    if (contractMode) {
                        currentNodeSet.remove(neighbour.getId());
                    } else {
                        currentNodeSet.add(neighbour.getId());
                    }
                }
                currentNodes.put(currentWorkspace, currentNodeSet);
            }
        }
        updateQuery(currentWorkspace);
    }

    /**
     * This class computes the Weakly Connected Components of a graph, it uses a Depth-First-Search based algorithm.
     */
    static class WeaklyConnectedComponent {
        private Set<Object> nodeVisited = new HashSet<>();
        private Graph graph;

        WeaklyConnectedComponent(Graph graph) {
            this.graph = graph;
        }

        private void dfsVisit(Node node, Set<Object> component) {
            nodeVisited.add(node.getId());
            component.add(node.getId());
            for (Node neighbor : graph.getNeighbors(node)) {
                if (!nodeVisited.contains(neighbor.getId())) {
                    dfsVisit(neighbor, component);
                }
            }

        }

        /**
         * Gets all components in the graph
         * @return A List of Node sets representing components
         */
        List<Set<Object>> getComponents() {
            // Use a DFS to find the weakly connected components
            List<Set<Object>> components = new ArrayList<>();
            for (Node node : graph.getNodes()) {
                if (!nodeVisited.contains(node.getId())) {
                    //Perform DFS and add all nodes to component
                    Set<Object> component = new HashSet<>();
                    dfsVisit(node, component);
                    components.add(component);
                }
            }
            return components;
        }

    }
}
