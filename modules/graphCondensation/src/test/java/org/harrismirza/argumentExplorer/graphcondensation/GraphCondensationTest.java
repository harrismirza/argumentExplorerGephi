package org.harrismirza.argumentExplorer.graphcondensation;

import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openide.util.Lookup;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GraphCondensationTest {

    private static Logger log = Logger.getAnonymousLogger();
    private ProjectController projectController;
    private int workspaceId;

    @BeforeEach
    public void setUpWorkspace() {
        projectController = Lookup.getDefault().lookup(ProjectController.class);
        projectController.newProject();
        Workspace workspace = projectController.newWorkspace(projectController.getCurrentProject());
        workspaceId = workspace.getId();
        projectController.openWorkspace(workspace);
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);
        Graph graph = graphModel.getGraph();

        //Create a graph
        GraphFactory graphFactory = graphModel.factory();

        Node node0 = graphFactory.newNode("0");
        Node node1 = graphFactory.newNode("1");
        Node node2 = graphFactory.newNode("2");
        Node node3 = graphFactory.newNode("3");
        Node node4 = graphFactory.newNode("4");
        Node node5 = graphFactory.newNode("5");
        Node node6 = graphFactory.newNode("6");
        Node node7 = graphFactory.newNode("7");
        Node node8 = graphFactory.newNode("8");

        Edge edge01 = graphFactory.newEdge(node0, node1);
        Edge edge12 = graphFactory.newEdge(node1, node2);
        Edge edge20 = graphFactory.newEdge(node2, node0);
        Edge edge23 = graphFactory.newEdge(node2, node3);
        Edge edge31 = graphFactory.newEdge(node3, node1);
        Edge edge43 = graphFactory.newEdge(node4, node3);
        Edge edge56 = graphFactory.newEdge(node5, node6);
        Edge edge64 = graphFactory.newEdge(node6, node4);
        Edge edge75 = graphFactory.newEdge(node7, node5);
        Edge edge78 = graphFactory.newEdge(node7, node8);
        Edge edge85 = graphFactory.newEdge(node8, node5);
        Edge edge87 = graphFactory.newEdge(node8, node7);

        graph.addNode(node0);
        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node4);
        graph.addNode(node5);
        graph.addNode(node6);
        graph.addNode(node7);
        graph.addNode(node8);

        graph.addEdge(edge01);
        graph.addEdge(edge12);
        graph.addEdge(edge20);
        graph.addEdge(edge23);
        graph.addEdge(edge31);
        graph.addEdge(edge43);
        graph.addEdge(edge56);
        graph.addEdge(edge64);
        graph.addEdge(edge75);
        graph.addEdge(edge78);
        graph.addEdge(edge85);
        graph.addEdge(edge87);

        //Add SCC attribute
        Column sccColumns = graphModel.getNodeTable().addColumn("scc_id", Integer.class);

        node0.setAttribute(sccColumns, 0);
        node1.setAttribute(sccColumns, 0);
        node2.setAttribute(sccColumns, 0);
        node3.setAttribute(sccColumns, 0);
        node4.setAttribute(sccColumns, 1);
        node5.setAttribute(sccColumns, 2);
        node6.setAttribute(sccColumns, 3);
        node7.setAttribute(sccColumns, 4);
        node8.setAttribute(sccColumns, 4);

    }

    @Test
    public void testCondensationAndNodeLevels() throws InterruptedException {
        GraphCondensationController graphCondensationController = new GraphCondensationController();

        graphCondensationController.condenseGraph(projectController.getCurrentWorkspace().getId(), "scc_id",
                new HashSet<>());

        while (workspaceId == projectController.getCurrentWorkspace().getId()) {
            Thread.sleep(100);
        }

        GraphModel condensationGraphModel =
                Lookup.getDefault().lookup(GraphController.class).getGraphModel(projectController.getCurrentWorkspace());

        assertEquals(5, condensationGraphModel.getGraph().getNodes().toCollection().size());

        List<String> nodeNames =
                condensationGraphModel.getGraph().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames = Arrays.asList("c-0", "c-1", "c-2", "c-3", "c-4");

        List<Map.Entry<String, String>> edges =
                condensationGraphModel.getDirectedGraph().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges = Arrays.asList(
                new AbstractMap.SimpleEntry<>("c-1", "c-0"),
                new AbstractMap.SimpleEntry<>("c-3", "c-1"),
                new AbstractMap.SimpleEntry<>("c-2", "c-3"),
                new AbstractMap.SimpleEntry<>("c-4", "c-2")
        );


        assert (nodeNames.containsAll(expectedNodeNames) && expectedNodeNames.containsAll(nodeNames));
        assert (edges.containsAll(expectedEdges) && expectedEdges.containsAll(edges));

        graphCondensationController.computeNodeLevels(projectController.getCurrentWorkspace().getId());

        assertEquals(2, condensationGraphModel.getGraph().getNode("c-0").getAttribute("componentlayer"));
        assertEquals(1, condensationGraphModel.getGraph().getNode("c-1").getAttribute("componentlayer"));
        assertEquals(1, condensationGraphModel.getGraph().getNode("c-2").getAttribute("componentlayer"));
        assertEquals(1, condensationGraphModel.getGraph().getNode("c-3").getAttribute("componentlayer"));
        assertEquals(0, condensationGraphModel.getGraph().getNode("c-4").getAttribute("componentlayer"));

    }


    @Test
    public void testCondensationWithIgnoredValues() throws InterruptedException {
        GraphCondensationController graphCondensationController = new GraphCondensationController();

        graphCondensationController.condenseGraph(projectController.getCurrentWorkspace().getId(), "scc_id",
                new HashSet<>(Arrays.asList(0, 4)));

        while (workspaceId == projectController.getCurrentWorkspace().getId()) {
            Thread.sleep(100);
        }

        GraphModel condensationGraphModel =
                Lookup.getDefault().lookup(GraphController.class).getGraphModel(projectController.getCurrentWorkspace());

        assertEquals(9, condensationGraphModel.getGraph().getNodes().toCollection().size());

        List<String> nodeNames =
                condensationGraphModel.getGraph().getNodes().toCollection().stream().map(node -> node.getId().toString()).collect(Collectors.toList());
        List<String> expectedNodeNames = Arrays.asList("0", "1", "2", "3", "c-1", "c-2", "c-3", "7", "8");

        List<Map.Entry<String, String>> edges =
                condensationGraphModel.getDirectedGraph().getEdges().toCollection().stream().map(edge -> new AbstractMap.SimpleEntry<>(edge.getSource().getId().toString(), edge.getTarget().getId().toString())).collect(Collectors.toList());
        List<Map.Entry<String, String>> expectedEdges = Arrays.asList(
                new AbstractMap.SimpleEntry<>("0", "1"),
                new AbstractMap.SimpleEntry<>("1", "2"),
                new AbstractMap.SimpleEntry<>("2", "0"),
                new AbstractMap.SimpleEntry<>("2", "3"),
                new AbstractMap.SimpleEntry<>("3", "1"),
                new AbstractMap.SimpleEntry<>("c-1", "3"),
                new AbstractMap.SimpleEntry<>("c-3", "c-1"),
                new AbstractMap.SimpleEntry<>("c-2", "c-3"),
                new AbstractMap.SimpleEntry<>("7", "c-2"),
                new AbstractMap.SimpleEntry<>("8", "c-2"),
                new AbstractMap.SimpleEntry<>("7", "8"),
                new AbstractMap.SimpleEntry<>("8", "7")
        );

        assert (nodeNames.containsAll(expectedNodeNames) && expectedNodeNames.containsAll(nodeNames));
        assert (edges.containsAll(expectedEdges) && expectedEdges.containsAll(edges));

    }

}