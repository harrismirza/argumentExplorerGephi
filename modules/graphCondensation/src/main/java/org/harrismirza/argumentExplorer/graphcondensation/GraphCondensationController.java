package org.harrismirza.argumentExplorer.graphcondensation;

import org.gephi.filters.api.FilterController;
import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.project.api.WorkspaceInformation;
import org.gephi.project.api.WorkspaceProvider;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a controller class for the Graph Condensation features, it stores data related to the condensation and performs operations
 */
@ServiceProvider(service = GraphCondensationController.class)
public class GraphCondensationController {
    Map<Integer, Integer> condensationMap = new HashMap<>();
    Map<Integer, String> condensationAttributeColumnMap = new HashMap<>();
    Map<Integer, Set<Object>> condensationComponentsMap = new HashMap<>();
    Map<Integer, Set<Object>> condensationExpandedComponentsMap = new HashMap<>();

    /**
     * This creates a new workspace containing a condensation of the provided workspace based on the provided attribute and expanding the ignored values
     * @param workspaceId The workspace to condense
     * @param selectedAttribute The attribute column to condense on
     * @param ignoredValues The values to ignore during condensation (these will be expanded)
     */
    void condenseGraph(int workspaceId, String selectedAttribute, Set<Object> ignoredValues) {
        System.out.println("Ignored Values");
        System.out.println(ignoredValues);

        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        WorkspaceProvider workspaceProvider =
                projectController.getCurrentProject().getLookup().lookup(WorkspaceProvider.class);
        Workspace originalWorkspace = workspaceProvider.getWorkspace(workspaceId);

        GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
        Graph originalGraph = graphController.getGraphModel(originalWorkspace).getGraph();
        originalGraph.readLock();
        // Get Selected Attribute Column by checking titles of all columns
        Column selectedAttributeColumn = null;
        for (Column column : graphController.getGraphModel(originalWorkspace).getNodeTable().toArray()) {
            if (column.getTitle().equals(selectedAttribute)) {
                selectedAttributeColumn = column;
                break;
            }
        }
        System.out.println("Selected Attribute Column: " + selectedAttributeColumn);
        // *********************************************************
        // 1. Create mapping of nodes to super nodes
        // Create mapping of nodes to super-nodes
        List<Object> nodesToKeep = new ArrayList<>();
        Map<Object, Set<Object>> condensationMapping = new HashMap<>();
        for (Node originalNode : originalGraph.getNodes().toArray()) {
            Object condensationAttribute = originalNode.getAttribute(selectedAttributeColumn);
            if (!ignoredValues.contains(condensationAttribute)) {
                condensationMapping.putIfAbsent(condensationAttribute, new HashSet<>());
                condensationMapping.get(condensationAttribute).add(originalNode.getId());
            } else {
                nodesToKeep.add(originalNode.getId());
            }
        }

        // *********************************************************
        // 2. Duplicate Workspace & add columns
        // Create New Graph
        originalGraph.readUnlockAll();
        Workspace condensationWorkspace = projectController.duplicateWorkspace(originalWorkspace);
        String workspaceName = String.format("Condensation of %s", originalWorkspace.getLookup().lookup(WorkspaceInformation.class).getName());
        if(!ignoredValues.isEmpty()){
            workspaceName += ", Components " + ignoredValues.stream().map(Object::toString)
                    .collect(Collectors.joining(", ")) + " expanded";
        }
        projectController.renameWorkspace(condensationWorkspace, workspaceName);
        originalGraph.readLock();
        System.out.println("New Workspace");
        GraphModel condensationGraphModel =
                Lookup.getDefault().lookup(GraphController.class).getGraphModel(condensationWorkspace);

        DirectedGraph condensationGraph = condensationGraphModel.getDirectedGraph();
        GraphFactory condensationGraphFactory = condensationGraphModel.factory();
        condensationGraphModel.getNodeTable().addColumn("ComponentSize", Integer.class);
        condensationGraphModel.getNodeTable().addColumn("ComponentValue", selectedAttributeColumn.getTypeClass());


        // *********************************************************
        // 3. Create new nodes in the graph
        // Create Super Nodes
        HashMap<Object, Object> nodeSuperNodeMap = new HashMap<>();
        condensationGraph.writeLock();
        for (Map.Entry<Object, Set<Object>> entry : condensationMapping.entrySet()) {
            String superNodePrefix = "c-" + entry.getKey().toString();
            int suffix = 0;
            String superNodeId = superNodePrefix;
            while (nodesToKeep.contains(superNodeId)) {
                superNodeId = superNodePrefix + "_" + suffix;
                suffix += 1;
            }
            Node superNode = condensationGraphFactory.newNode("c-" + entry.getKey().toString());
            superNode.setLabel("c-" + entry.getKey().toString());
            // Get average x and y coords
            float totalX = 0;
            float totalY = 0;
            int numSubNodes = entry.getValue().size();
            for (Object nodeId : entry.getValue()) {
                Node subNode = originalGraph.getNode(nodeId);
                totalX += subNode.x();
                totalY += subNode.y();
            }
            superNode.setX(totalX / numSubNodes);
            superNode.setY(totalY / numSubNodes);
            superNode.setSize(10.0f);
            superNode.setAttribute("ComponentSize", entry.getValue().size());
            superNode.setAttribute("ComponentValue", selectedAttributeColumn.getTypeClass().cast(entry.getKey()));
            condensationGraph.addNode(superNode);

            for (Object nodeId : entry.getValue()) {
                nodeSuperNodeMap.put(nodeId, superNode.getId());
            }
        }
        for (Object nodeId : nodesToKeep) {
            Node newNode = condensationGraphFactory.newNode(nodeId.toString());
            Node originalNode = originalGraph.getNode(nodeId.toString());
            newNode.setLabel(nodeId.toString());
            newNode.setX(originalNode.x());
            newNode.setY(originalNode.y());
            newNode.setSize(10.0f);
            newNode.setAttribute("ComponentSize", 0);
            newNode.setAttribute("ComponentValue", null);
            condensationGraph.addNode(newNode);
            nodeSuperNodeMap.put(nodeId, newNode.getId());
        }
        originalGraph.readUnlockAll();
        condensationGraph.writeUnlock();
        System.out.println("Created Nodes");

        // *********************************************************
        // 4. Create new edges in the graph
        // Create Edges

        for (Edge edge : originalGraph.getEdges().toArray()) {
            condensationGraph.readLock();
            Node superNodeSource = condensationGraph.getNode(nodeSuperNodeMap.get(edge.getSource().getId()));
            Node superNodeTarget = condensationGraph.getNode(nodeSuperNodeMap.get(edge.getTarget().getId()));
            condensationGraph.readUnlockAll();
            if (!superNodeSource.equals(superNodeTarget) && condensationGraph.getEdge(superNodeSource,
                    superNodeTarget) == null) {
                Edge newEdge = condensationGraphFactory.newEdge(superNodeSource, superNodeTarget);
                condensationGraph.addEdge(newEdge);
            }
        }

        System.out.println("Created Edges");
        // Add attributes for tracking
        condensationMap.put(condensationWorkspace.getId(), originalWorkspace.getId());
        condensationAttributeColumnMap.put(condensationWorkspace.getId(), selectedAttributeColumn.getId());
        condensationComponentsMap.put(condensationWorkspace.getId(), condensationMapping.keySet());
        condensationExpandedComponentsMap.put(condensationWorkspace.getId(), ignoredValues);
        System.out.println("Set Tracking Attributes");



    }

    /**
     * This computes the Node Decomposition of a workspace that is a condensation
     * @param condensationWorkspaceId The id of the condensation workspace
     */
    void computeNodeLevels(int condensationWorkspaceId) {
        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
        WorkspaceProvider workspaceProvider =
                projectController.getCurrentProject().getLookup().lookup(WorkspaceProvider.class);
        GraphModel condensationGraphModel =
                graphController.getGraphModel(workspaceProvider.getWorkspace(condensationWorkspaceId));
        DirectedGraph condensationGraph = condensationGraphModel.getDirectedGraph();
        condensationGraph.readLock();
        if (condensationGraphModel.getNodeTable().getColumn("componentlayer") == null) {
            condensationGraphModel.getNodeTable().addColumn("ComponentLayer", Integer.class);
        }

        // Find trivial components
        int originalWorkspaceId = condensationMap.get(condensationWorkspaceId);
        GraphModel originalGraphModel =
                graphController.getGraphModel(workspaceProvider.getWorkspace(originalWorkspaceId));
        DirectedGraph originalGraph = originalGraphModel.getDirectedGraph();
        originalGraph.readLock();

        String condensationAttributeId = condensationAttributeColumnMap.get(condensationWorkspaceId);

        // First find all components with size of 1 and get the component value
        Set<Object> singleComponentValues = condensationGraph.getNodes().toCollection().stream()
                .filter(node -> (int) node.getAttribute("componentsize") == 1)
                .map(node -> node.getAttribute("componentvalue"))
                .collect(Collectors.toSet());

        System.out.println("Single component values\n======================");
        System.out.println(singleComponentValues);
        // Then find the nodes in the graph with those values that don't have self loops and get their values
        Set<Object> trivialNodeValues = originalGraph.getNodes().toCollection().stream()
                .filter(node -> singleComponentValues.contains(node.getAttribute(condensationAttributeId)))
                .filter(node -> !originalGraph.getNeighbors(node).toCollection().contains(node))
                .map(node -> node.getAttribute(condensationAttributeId))
                .collect(Collectors.toSet());

        // The find all components with those values
        Set<Object> trivialComponents = condensationGraph.getNodes().toCollection().stream()
                .filter(node -> trivialNodeValues.contains(node.getAttribute("componentvalue")))
                .map(Element::getId)
                .collect(Collectors.toSet());

        Map<Object, Integer> nodeLevels = new GraphCondensationController.NodeLevelComputation(condensationGraph,
                trivialComponents).getNodeLevels();
        nodeLevels.forEach((nodeId, level) -> condensationGraph.getNode(nodeId).setAttribute("ComponentLayer", level));
        originalGraph.readUnlockAll();
        condensationGraph.readUnlockAll();
    }

    /**
     * Finds the original workspace and displays it, filtered to show only the elements from one component
     * @param componentValues The values to filter by
     */
    void viewComponents(List<Object> componentValues) {
        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        WorkspaceProvider workspaceProvider =
                projectController.getCurrentProject().getLookup().lookup(WorkspaceProvider.class);
        Workspace workspace = projectController.getCurrentWorkspace();

        // Find the original workspace
        int originalWorkspaceId = condensationMap.get(workspace.getId());
        String originalWorkspaceCondensationAttributeId = condensationAttributeColumnMap.get(workspace.getId());

        System.out.println("Original Workspace ID " + originalWorkspaceId);
        System.out.println(Arrays.toString(workspaceProvider.getWorkspaces()));

        // Switch to the original workspace
        Workspace originalWorkspace = workspaceProvider.getWorkspace(originalWorkspaceId);
        projectController.openWorkspace(originalWorkspace);
        FilterController filterController = Lookup.getDefault().lookup(FilterController.class);

        Column filterColumn =
                Lookup.getDefault().lookup(GraphController.class).getGraphModel().getNodeTable().getColumn(originalWorkspaceCondensationAttributeId);
        NodeAttributeFilterBuilder filterBuilder = new NodeAttributeFilterBuilder();

        // Set up filter to show only nodes with that value
        filterBuilder.setOptions(filterColumn, componentValues);

        filterController.filterVisible(filterController.createQuery(filterBuilder.getFilter(originalWorkspace)));
    }

    /**
     * This class computes node decomposition levels
     */
    private static class NodeLevelComputation {

        private Map<Object, Integer> levels;
        private DirectedGraph condensationGraph;
        private Set<Object> trivialNodes;

        private NodeLevelComputation(DirectedGraph condensationGraph, Set<Object> trivialNodes) {
            this.condensationGraph = condensationGraph;
            this.trivialNodes = trivialNodes;
        }

        /**
         * Compute the decomposition level of the provided node
         * @param node The node to find the level for
         * @return The decomposition level
         */
        private int computeNodeLevel(Node node) {
            // If the level is already computed, return it
            if (levels.containsKey(node.getId())) {
                return levels.get(node.getId());
            }
            // If the node has no in edges, it's level is 0
            if (condensationGraph.getInDegree(node) == 0) {
                levels.put(node.getId(), 0);
                return 0;
            } else {
                Collection<Node> parents = condensationGraph.getPredecessors(node).toCollection();
                // Recursively compute levels of parents if missing
                if (trivialNodes.contains(node.getId())) {
                    // Find all parents of trivial nodes and find the max level
                    int level = parents.stream().map(o -> {
                        int _level = computeNodeLevel(o);
                        // If parent is trivial, add one to level
                        if (!trivialNodes.contains(o.getId())) {
                            _level += 1;
                        }
                        return _level;
                    }).max(Integer::compareTo).get();
                    levels.put(node.getId(), level);
                    return level;
                } else {
                    // Find all parents of non-trivial nodes and find the max level
                    int level = parents.stream().map(o -> computeNodeLevel(o) + 1)
                            .max(Integer::compareTo).get();
                    levels.put(node.getId(), level);
                    return level;
                }


            }
        }

        /**
         * This computes and returns the decomposition level for all nodes
         * @return A mapping from the node ids to the computed levels
         */
        private Map<Object, Integer> getNodeLevels() {
            levels = new HashMap<>();
            System.out.println("Trivial Nodes " + trivialNodes.toString());
            // Loop over all nodes and find their level
            for (Node node : condensationGraph.getNodes().toCollection()) {
                if (!levels.containsKey(node.getId())) {
                    levels.put(node.getId(), computeNodeLevel(node));
                }
            }

            return levels;
        }

    }
}
