package org.harrismirza.argumentExplorer.graphcondensation;

import org.gephi.filters.api.FilterLibrary;
import org.gephi.filters.spi.*;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.project.api.Workspace;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;
import java.util.List;

/**
 * This class creates instances of filters that filter nodes with a specific value in an attribute column
 */
@ServiceProvider(service = FilterBuilder.class)
public class NodeAttributeFilterBuilder implements FilterBuilder {

    Column comparisonColumn;
    List<Object> filterValues;

    public void setOptions(Column column, List<Object> value) {
        this.comparisonColumn = column;
        this.filterValues = value;

        System.out.println(column);
        System.out.println(value);
    }

    @Override
    public Category getCategory() {
        return FilterLibrary.ATTRIBUTES;
    }

    @Override
    public String getName() {
        return "Node Attribute Filter";
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getDescription() {
        return "A node attribute filter";
    }

    @Override
    public Filter getFilter(Workspace workspace) {
        return new NodeFilter() {
            @Override
            public boolean init(Graph graph) {
                return true;
            }

            @Override
            public boolean evaluate(Graph graph, Node node) {
                return filterValues.contains(node.getAttribute(comparisonColumn));
            }

            @Override
            public void finish() {

            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public FilterProperty[] getProperties() {
                return new FilterProperty[0];
            }
        };
    }

    @Override
    public JPanel getPanel(Filter filter) {
        return null;
    }

    @Override
    public void destroy(Filter filter) {

    }
}
