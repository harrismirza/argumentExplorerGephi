package org.harrismirza.argumentExplorer.graphcondensation;

import org.gephi.graph.api.Column;
import org.gephi.graph.api.GraphController;
import org.gephi.project.api.*;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;


/**
 * This class controls the panel used for the Graph Condensation functionality
 */
@TopComponent.Description(preferredID = "GraphCondensationTopComponent")
@TopComponent.Registration(mode = "rankingmode", openAtStartup = true)
@ActionID(category = "Window", id = "org.harrismirza.argumentExplorer.GraphCondensationTopComponent")
@ActionReference(path = "Menu/Window", position = 333)
@TopComponent.OpenActionRegistration(displayName = "Graph Condensation", preferredID =
        "GraphCondensationTopComponent")
public class GraphCondensationTopComponent extends TopComponent {
    private GraphCondensationController condensationController;

    private JLabel workspaceNameLabel;
    private JComboBox<String> attributeComboBox;
    private JList<Object> componentList;
    private JButton viewComponentButton;
    private JButton toggleExpandComponentButton;
    private JButton nodeLevelCalculationButton;

    public GraphCondensationTopComponent() {
        super();
        initComponents();
        setName("Graph Condensation");
        setToolTipText("Use this panel to control the condensation of the graph.");

        condensationController = Lookup.getDefault().lookup(GraphCondensationController.class);

    }

    /**
     * Creates the components for the User Interface
     */
    private void initComponents() {
        // Define Layout
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);


        //Row 0 - Label
        workspaceNameLabel = new JLabel("Current Workspace:");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 3;
        gbc.weightx = 3;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(workspaceNameLabel, gbc);

        //Row 1 - Attribute Box
        attributeComboBox = new JComboBox<>();
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 2;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(attributeComboBox, gbc);

        //Row 2 - Condensation Buttons
        JButton attributeRefreshButton = new JButton("Refresh");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(attributeRefreshButton, gbc);
        JButton condenseButton = new JButton("Condense");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(condenseButton, gbc);

        //Row 3 - Component List
        JScrollPane componentListScrollPane = new JScrollPane();
        gbc.gridheight = 1;
        gbc.weighty = 100;
        gbc.gridwidth = 2;
        gbc.weightx = 2;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(componentListScrollPane, gbc);

        //Row 4 - Component Buttons
        viewComponentButton = new JButton("View Component");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(viewComponentButton, gbc);
        toggleExpandComponentButton = new JButton("Expand/Unexpand Component");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(toggleExpandComponentButton, gbc);
        nodeLevelCalculationButton = new JButton("Calculate Node levels");
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 2;
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.PAGE_START;
        this.add(nodeLevelCalculationButton, gbc);

        // Create attribute model
        attributeComboBox.setModel(new DefaultComboBoxModel<>());

        // Add List and cell renderer
        componentList = new JList<>();
        ListCellRenderer<Object> defaultRenderer = new DefaultListCellRenderer();
        componentList.setCellRenderer((list, value, index, isSelected, cellHasFocus) -> {
            // Get the normal label
            JLabel defaultLabel = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                    isSelected, cellHasFocus);
            // Find if this component is expanded and if so, add a suffix
            Set<Object> expandedValues =
                    condensationController.condensationExpandedComponentsMap.getOrDefault(
                            Lookup.getDefault().lookup(ProjectController.class).getCurrentWorkspace().getId(),
                            Collections.emptySet());
            if (expandedValues.contains(value)) {
                defaultLabel.setText(defaultLabel.getText() + " (Expanded)");
            }
            return defaultLabel;
        });

        componentListScrollPane.setViewportView(componentList);


        // Add button listeners
        condenseButton.addActionListener(e -> {
            //Condense graph
            condensationController.condenseGraph(
                    Lookup.getDefault().lookup(ProjectController.class).getCurrentWorkspace().getId(),
                    (String) attributeComboBox.getSelectedItem(),
                    new HashSet<>());
        });

        attributeRefreshButton.addActionListener(e -> refreshWorkspace());

        viewComponentButton.addActionListener(e -> condensationController.viewComponents(componentList.getSelectedValuesList()));

        toggleExpandComponentButton.addActionListener(e -> {
            // Expand or Contract a node
            ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
            WorkspaceProvider workspaceProvider =
                    projectController.getCurrentProject().getLookup().lookup(WorkspaceProvider.class);
            Workspace workspace = projectController.getCurrentWorkspace();

            int originalWorkspaceId = condensationController.condensationMap.get(workspace.getId());
            String originalWorkspaceCondensationAttributeId =
                    condensationController.condensationAttributeColumnMap.get(workspace.getId());
            String originalWorkspaceCondensationAttribute =
                    Lookup.getDefault().lookup(GraphController.class).getGraphModel(
                            workspaceProvider.getWorkspace(originalWorkspaceId)).getNodeTable()
                            .getColumn(originalWorkspaceCondensationAttributeId).getTitle();

            Set<Object> expandedComponentSet =
                    condensationController.condensationExpandedComponentsMap.get(workspace.getId());
            expandedComponentSet = new HashSet<>(expandedComponentSet);

            List<Object> expansionList = componentList.getSelectedValuesList();

            for (Object component : expansionList) {
                // Toggle the node in the expanded component set
                if (expandedComponentSet.contains(component)) {
                    expandedComponentSet.remove(component);
                } else {
                    expandedComponentSet.add(component);
                }
            }

            // Recondense the original graph with the new set of expanded nodes
            condensationController.condenseGraph(originalWorkspaceId, originalWorkspaceCondensationAttribute,
                    expandedComponentSet);

        });

        nodeLevelCalculationButton.addActionListener(e -> {
            ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
            Workspace workspace = projectController.getCurrentWorkspace();
            condensationController.computeNodeLevels(workspace.getId());
        });

        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        // Whenever the workspace changes refresh the UI
        projectController.addWorkspaceListener(new WorkspaceListener() {
            @Override
            public void initialize(Workspace workspace) {
            }

            @Override
            public void select(Workspace workspace) {
                SwingUtilities.invokeLater(() -> refreshWorkspace());
            }

            @Override
            public void unselect(Workspace workspace) {
            }

            @Override
            public void close(Workspace workspace) {
            }

            @Override
            public void disable() {
            }
        });
    }

    /**
     * Refreshes the UI components with data from the current workspace
     */
    private void refreshWorkspace() {
        GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
        if (graphController == null || graphController.getGraphModel() == null) {
            return;
        }
        Set<String> attributeNames = new HashSet<>();
        for (Column column : graphController.getGraphModel().getNodeTable()) {
            attributeNames.add(column.getTitle());
        }
        // Update node attribute list
        attributeComboBox.setModel(new DefaultComboBoxModel<>(attributeNames.toArray(new String[0])));

        ProjectController projectController = Lookup.getDefault().lookup(ProjectController.class);
        Workspace workspace = projectController.getCurrentWorkspace();

        workspaceNameLabel.setText(String.format("Current Workspace: %s", workspace.getLookup().lookup(WorkspaceInformation.class).getName()));

        // Update list of components if this is a condensation
        if (condensationController.condensationMap.containsKey(workspace.getId())) {
            Set<Object> condensationComponentValues =
                    condensationController.condensationComponentsMap.get(workspace.getId());
            Set<Object> expandedComponentValues =
                    condensationController.condensationExpandedComponentsMap.get(workspace.getId());

            Object[] components = Stream.concat(expandedComponentValues.stream(),
                    condensationComponentValues.stream()).toArray();
            componentList.setListData(components);
            componentList.setEnabled(true);
            viewComponentButton.setEnabled(true);
            toggleExpandComponentButton.setEnabled(true);
            // Only enable the node level calculation if no elements are expanded
            if (condensationController.condensationExpandedComponentsMap.get(workspace.getId()).size() == 0) {
                nodeLevelCalculationButton.setEnabled(true);
            } else {
                nodeLevelCalculationButton.setEnabled(false);
            }
        } else {
            // If this is not a condensation, clear the list and disable the UI elements
            componentList.setListData(new Object[0]);
            componentList.setEnabled(false);
            viewComponentButton.setEnabled(false);
            toggleExpandComponentButton.setEnabled(false);
            nodeLevelCalculationButton.setEnabled(false);
        }
    }
}

